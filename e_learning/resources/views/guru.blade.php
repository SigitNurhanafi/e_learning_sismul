@extends('layouts.guru.master')
@section('title', 'Dashboard')

@section('content')
  <div class="content">
      <div class="">
        <div class="page-header-title">
          <h4 class="page-title">Wellcome Guru E-Learning</h4></div>
      </div>
      <div class="page-content-wrapper ">
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <div class="panel panel-primary">
                <div class="panel-body">
                  <h4 class="m-t-0">Hi, Guru E-learning , Nice to meet youuu...</h4>
                  <div style="height: 300px"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
