@extends('layouts.admin.master') @section('title', 'Form Tambah Guru') @section('content')
<div class="content">
  <div class="">
    <div class="page-header-title">
      <h4 class="page-title">Tambah Siswa</h4>
    </div>

  </div>
  <div class="page-content-wrapper ">
    <div class="container">
      <div class="row">
        @if (session('msg'))
        <br>
        <div class="alert alert-warning alert-dismissible fade in">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <p>{!!session('msg')!!} </p>
        </div>
        @endif @if (count($errors) > 0)
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li> {{$error}} </li>
            @endforeach
          </ul>
        </div>
        @endif
        <div class="row">
          <div class="col-md-5">
            <div class="panel panel-primary">
              <div class="panel-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ route('create-tambah-guru') }}">
                  {{ csrf_field() }}

                  <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">Name</label>

                    <div class="col-md-6">
                      <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"> @if ($errors->has('name'))
                      <span class="help-block">
                                      <strong>{{ $errors->first('name') }}</strong>
                                  </span> @endif
                    </div>
                  </div>

                  <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                    <div class="col-md-6">
                      <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"> @if ($errors->has('email'))
                      <span class="help-block">
                                      <strong>{{ $errors->first('email') }}</strong>
                                  </span> @endif
                    </div>
                  </div>

                  <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Password</label>

                    <div class="col-md-6">
                      <input id="password" type="password" class="form-control" name="password"> @if ($errors->has('password'))
                      <span class="help-block">
                                      <strong>{{ $errors->first('password') }}</strong>
                                  </span> @endif
                    </div>
                  </div>

                  <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                    <div class="col-md-6">
                      <input id="password-confirm" type="password" class="form-control" name="password_confirmation"> @if ($errors->has('password_confirmation'))
                      <span class="help-block">
                                      <strong>{{ $errors->first('password_confirmation') }}</strong>
                                  </span> @endif
                    </div>
                  </div>

                  <div class="form-group{{ $errors->has('mata_pelajaran') ? ' has-error' : '' }}">
                    <label for="password-confirm" class="col-md-4 control-label">Jurusan</label>

                    <div class="col-md-6">
                      <select class="form-control" name="jurusan">
                                <option value=""> - pilih - </option>
                                <option value="IPA">IPA</option>
                                <option value="IPS">IPS</option>
                            </select> @if ($errors->has('password_confirmation'))
                      <span class="help-block">
                                      <strong>{{ $errors->first('password_confirmation') }}</strong>
                                  </span> @endif
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                      <button type="submit" class="btn btn-primary">
                                  <i class="fa fa-btn fa-user"></i> Tambahkan
                              </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>

          </div>
          <div class="col-md-7">
            <div class="panel panel-primary">
              <div class="panel-body">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Nama Siswa</th>
                      <th>Email</th>
                      <th>Jurusan</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach ($UserSiswa as $key => $value)
                    <tr>
                      <td>{{$value->name}}</td>
                      <td>{{$value->email}}</td>
                      <td>{{$value->jurusan}}</td>
                      <td><button type="button" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal-edit-{{$value->id}}">Edit</button> <button type="button" class="btn btn-danger waves-effect waves-light" data-toggle="modal" data-target="#modal-delete-{{$value->id}}">Delete</button></td>
                    </tr>
                  @endforeach


                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>
</div>
@endsection
