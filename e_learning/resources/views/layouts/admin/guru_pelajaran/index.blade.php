@extends('layouts.admin.master')
@section('title', 'Olah Guru Mata Pelajaran')
@section('content')
  <div class="content">
      <div class="">
        <div class="page-header-title">
          <h4 class="page-title">Olah Data Mata Pelajaran</h4></div>
      </div>
      <div class="page-content-wrapper ">
        <div class="container">
          <div class="row">
            @if (session('msg'))
              <br>
              <div class="alert alert-warning alert-dismissible fade in">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>  <p>{!!session('msg')!!} </p></div>
            @endif
            @if (count($errors) > 0)
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li> {{$error}} </li>
                  @endforeach
                </ul>
              </div>
            @endif
            <div class="row">
                        <div class="col-md-8">
                          <div class="panel panel-primary">
                            <div class="panel-body">
                              <h4 class="m-b-30 m-t-0"></h4>
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="table-responsive">
                                    <table class="table">
                                      <thead>
                                        <tr>
                                          <th>Mata Pelajaran</th>
                                          <th>Jurusan</th>
                                          <th>Nama Guru</th>
                                          <th>Aksi</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach ($guru as $key => $value)
                                          <tr>
                                            <td>{{$value->name}}</td>
                                            <td>{{$value->jurusan}}</td>
                                            <td>{{$value->nama_matpel}}</td>
                                            <td><button type="button" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal-edit-{{$value->id}}" >Edit</button> <button type="button" class="btn btn-danger waves-effect waves-light" data-toggle="modal" data-target="#modal-delete-{{$value->id}}">Delete</button></td>
                                          </tr>
                                        @endforeach

                                        @foreach ($guru as $key => $value)
                                        <div id="modal-edit-{{$value->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-edit-{{$value->id}}" aria-hidden="true" style="display: none">
                                          <div class="modal-dialog" style="width:30%">
                                            <div class="modal-content">
                                              <form class="" action="{{route('guru-pelajaran-update', ['id' => $value->id])}}" method="post">
                                                {{ csrf_field() }}
                                              <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="modal-delete-mapel-">Edit Mata Pelajaran</h4></div>
                                                <div class="modal-body">
                                                  <div class="panel-body ">
                                                    <div class="input-group">
                                                      <span class="input-group-addon"><i class="ion-android-book"></i></span>
                                                      <input type="text" class="form-control" name="nama_guru" placeholder="Nama Mata Pelajaran " value="{{$value->name}}">
                                                    </div>
                                                    <div class="jurusan" style="margin-bottom:10px;margin-top:10px;">
                                                      <span class="input-group-addon"><label>Jurusan :</label>
                                                        <label class="radio-inline"><input type="radio" name="jurusan" value="IPA" @if ($value->jurusan == 'IPA') checked @endif>IPA</label>
                                                        <label class="radio-inline"><input type="radio" name="jurusan" value="IPS" @if ($value->jurusan == 'IPS' ) checked @endif>IPS</label>
                                                      </span>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="modal-footer">
                                                  <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                  <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                    <span class="glyphicon glyphicon-floppy-disk"></span> Save changes</button>
                                                  </div>
                                                  </form>
                                                </div>
                                              </div>
                                            </div>
                                          @endforeach

                                          @foreach ($guru as $key => $value)
                                          <div id="modal-delete-{{$value->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-delete-{{$value->id}}" aria-hidden="true" style="display: none">
                                            <div class="modal-dialog" style="width:30%">
                                              <div class="modal-content">
                                                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                  <h4 class="modal-title" id="modal-delete-mapel-">Anda Yakin ?</h4></div>
                                                  <div class="modal-body">
                                                  Anda Akan Menghapus Permanen Data "<b>{{title_case($value->name)}} - {{strtoupper($value->jurusan)}} - {{strtoupper($value->nama_matpel)}}</b>" ?
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> <a href="{{route('guru-pelajaran-delete', ['id'=>$value->id])}}"> <button type="button" class="btn btn-primary waves-effect waves-light"><span class="glyphicon glyphicon-floppy-disk"></span> Delete</button></a></div>
                                                  </div>
                                                </div>
                                              </div>
                                            @endforeach

                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                <div class="panel text-center">
                  <div class="panel-heading">
                    <h4 class="panel-title text-muted font-light">Tambah Guru Mata Pelajaran</h4></div>
                  <div class="panel-body p-t-10">

                    <form class="form-horizontal" role="form" method="POST" action="{{ route('create-tambah-siswa') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('mata_pelajaran') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Mata Pelajaran</label>

                            <div class="col-md-6">
                              <select class="form-control" name="mata_pelajaran">
                                  <option value=""> - pilih - </option>
                                @foreach ($mata_pelajaran as $key => $value)
                                  <option value="{{ $value->id_matpel}}" > {{$value->nama_matpel}} ( {{$value->jurusan}} )</option>
                                @endforeach

                              </select>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Tambahkan
                                </button>
                            </div>
                        </div>
                    </form>

                  </div>
                </div>
              </div>
                      </div>

          </div>
        </div>
      </div>
    </div>

@endsection
