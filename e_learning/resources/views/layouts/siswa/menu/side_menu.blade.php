<div class="left side-menu">
      <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 176px;"><div class="sidebar-inner slimscrollleft" style="overflow: hidden; width: auto; height: 176px;">
        <div id="sidebar-menu">
          <ul>
            <li class="menu-title">hi , {{$name}}</li>
            <li>
              <a href="{{route('ujian')}}" class="waves-effect">
              <i class="ion-compose"></i>
              <span> Ujian Test
                {{-- <span class="badge badge-primary pull-right">1</span> --}}
              </span>
              </a>
            </li>
            <li>
              <a href="{{route('materi')}}" class="waves-effect">
              <i class="ion-archive"></i>
              <span> Materi Pelajaran
                {{-- <span class="badge badge-primary pull-right">1</span> --}}
              </span>
              </a>
            </li>

          </ul>
        </div>
        <div class="clearfix"></div>
      </div><div class="slimScrollBar" style="background: rgb(187, 187, 187); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 42.7845px; visibility: visible;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
    </div>
