@extends('layouts.siswa.ujian')
@section('title', 'Olah Mata Pelajaran')
@section('content')
@section('script')
    <script type="text/javascript">
            $("#uploadTrigger").click(function(){
             $("#uploadFile").click();
            });
        </script>
        		<script>
    		// Set the date we're counting down to
        @foreach ($soals as $key => $value)
    		var countDownDate = new Date("{{$value->end_ujian}}").getTime();
        @endforeach
    		// Update the count down every 1 second
    		var x = setInterval(function() {

    		    // Get todays date and time
    		    var now = new Date().getTime();

    		    // Find the distance between now an the count down date
    		    var distance =  countDownDate - now ;

    		    // Time calculations for days, hours, minutes and seconds
    		    // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    		    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    		    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    		    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    		    // Output the result in an element with id="demo"
    		    document.getElementById("demo").innerHTML = hours + " : "
    		    + minutes + " : " + seconds;
    		       // document.getElementById("demo").innerHTML = "sekarang :"+now+ "- " +countDownDate;

    		    // If the count down is over, write some text
    		    if (distance < 0) {
    		        document.getElementById("demo").innerHTML = "Upss Waktu Habis !";
                var curent_url = "{{Request::url()}}";
                idSoal = curent_url.split('/').slice(-1)[0]
                window.location = "{{asset('/siswa/lihat_nilai')}}" + "/" + idSoal;
    		    }
    		}, 1000);
    		</script>
@endsection
  <div class="content">
      <div class="">
        <li class="menu-title">Nama : {{$name}}</li>
        <div class="page-header-title">

      </div>
      <div class="page-content-wrapper ">
        <div class="container">
          <div class="row">
                    @if (session('msg'))
                      <br>
                      <div class="alert alert-warning alert-dismissible fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>  <p>{!!session('msg')!!} </p></div>
                    @endif
                    @if (count($errors) > 0)
                      <div class="alert alert-danger">
                        <ul>
                          @foreach ($errors->all() as $error)
                            <li> {{$error}} </li>
                          @endforeach
                        </ul>
                      </div>
                    @endif
            <div class="row">
                        <div class="col-md-9">
                          <div class="panel panel-primary">
                            <div class="panel-body">
                              <h4 class="m-b-30 m-t-0"></h4>
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="table-responsive">


                                        @foreach ($soals as $key => $value)
                                        <form class="" action="{{route('jawab-soal',['id' => $value->id])}}" method="post">
                                          {{csrf_field()}}
                                          <tr>
                                            <center>
                                              <h4 class="page-title">Mata Pelajaran : {{title_case($value->nama_matpel)}} </h4>
                                              <b> <span class="label label-danger " id="demo" style="font-size: 18px;">Memerikasa Waktu Dimulai</span>	</b>
                                              <hr>
                                            </center>
                                              <input type="hidden" name="id_random" value="{{$value->id_random}}">
                                              <input type="hidden" name="id_jawaban" value="{{$value->id_jawaban}}">
                                              <input type="hidden" name="id_ujian" value="{{$value->id_ujian}}">
                                              <input type="hidden" name="kunci_jawaban" value="{{$value->kunci_jawaban}}">
                                              <td> SOAL : </td>
                                              <br>
                                              @if ($value->file != NULL )
                                              <center><img width="60%" src="{{asset('/storage/upload/soal')}}/{{$value->file }}" alt="file {{$value->pertanyaan}}"></center>
                                              @endif
                                              <td>{{strtoupper($value->pertanyaan)}}</td>
                                              <br>
                                              <td><label class="radio-inline"><input type="radio" name="jawaban" value="A" <?php
                                                    if ($value->jawaban_siswa == "A") {
                                                        echo "checked";
                                                    }
                                                  ?>>A. {{strtoupper($value->jawaban_a)}}</label></td>
                                              <br>
                                              <td><label class="radio-inline"><input type="radio" name="jawaban" value="B" <?php
                                                    if ($value->jawaban_siswa == "B") {
                                                        echo "checked";
                                                    }
                                                  ?>>B. {{strtoupper($value->jawaban_b)}}</label></td>
                                              <br>
                                              <td><label class="radio-inline"><input type="radio" name="jawaban" value="C" <?php
                                                    if ($value->jawaban_siswa == "C") {
                                                        echo "checked";
                                                    }
                                                  ?>>C. {{strtoupper($value->jawaban_c)}}</label></td>
                                              <br>
                                              <td><label class="radio-inline"><input type="radio" name="jawaban" value="D" <?php
                                                    if ($value->jawaban_siswa == "D") {
                                                        echo "checked";
                                                    }
                                                  ?>>D. {{strtoupper($value->jawaban_d)}}</label></td>
                                              <br>
                                              <td><label class="radio-inline"><input type="radio" name="jawaban" value="E" <?php
                                                    if ($value->jawaban_siswa == "E") {
                                                        echo "checked";
                                                    }
                                                  ?>>E. {{strtoupper($value->jawaban_e)}}</label></td>
                                              <hr>
                                              <div class="box-footer">

                                                  <button class="btn btn-primary waves-effect waves-light" onclick="add_data_matpel();" name="simpan_data"><span class="glyphicon glyphicon-floppy-disk"></span>Reset</button>
                                                  <button type="submit" class="btn btn-primary waves-effect waves-light" onclick="" name="reset"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save Data</button>
                                              </div>
                                          <div>
                                            <?php echo str_replace('/?', '?', $soals->render()); ?>
                                          </div>
                                        </form>
                                        @endforeach
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
          </div>
        </div>
      </div>
    </div>

@endsection
