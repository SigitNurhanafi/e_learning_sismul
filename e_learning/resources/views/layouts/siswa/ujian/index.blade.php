@extends('layouts.siswa.master')
@section('title', 'Olah Mata Pelajaran')
@section('content')
  <div class="content">
      <div class="">
        <div class="page-header-title">
          <center>
          <h4 class="page-title">Silahkan Pilih Ujian</h4>
        </center>
        </div>

      </div>
      <div class="page-content-wrapper ">
        <div class="container">
          <div class="row">
                    @if (session('msg'))
                      <br>
                      <div class="alert alert-warning alert-dismissible fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>  <p>{!!session('msg')!!} </p></div>
                    @endif
                    @if (count($errors) > 0)
                      <div class="alert alert-danger">
                        <ul>
                          @foreach ($errors->all() as $error)
                            <li> {{$error}} </li>
                          @endforeach
                        </ul>
                      </div>
                    @endif
            <div class="row">
                        <div class="col-md-12">
                          <div class="panel panel-primary">
                            <div class="panel-body">
                              <h4 class="m-b-30 m-t-0"></h4>
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="table-responsive">
                                    <table class="table">
                                      <thead>
                                        <tr>

                                          <th>Nama Mata Pelajaran</th>
                                          <th>Judul Ujian</th>
                                          <th>Durasi</th>
                                          <th>Jumlah Soal</th>
                                          <th>Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach ($mata_pelajarans as $key => $value)
                                          <tr>
                                            <td>{{title_case($value->nama_matpel)}}</td>
                                            <td>{{strtoupper($value->nama_ujian)}}</td>
                                            <td>{{strtoupper($value->durasi)}} Menit</td>
                                            <td>{{strtoupper($value->jumlah_soal)}} Soal</td>
                                            <td>
                                              <a href="{{route('ujian-mulai',['id' => $value->id_ujian])}}"><button type="button" class="btn btn-warning waves-effect waves-light"  >Kerjakan</button></a>

                                          </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="page-header-title">
                            <center>
                            <h4 class="page-title">Ujian Yang Sudah Diikuti</h4>
                          </center>
                          </div>
                          <div class="panel panel-primary">
                            <div class="panel-body">
                              <h4 class="m-b-30 m-t-0"></h4>
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="table-responsive">
                                    <table class="table">
                                      <thead>
                                        <tr>
                                          <th>Nama Mata Pelajaran</th>
                                          <th>Judul Ujian</th>
                                          <th>Durasi</th>
                                          <th>Jumlah Soal</th>
                                          <th>Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach ($sudah_mengikuti_ujian as $key => $value)
                                          <tr>
                                            <td>{{title_case($value->nama_matpel)}}</td>
                                            <td>{{strtoupper($value->nama_ujian)}}</td>
                                            <td>{{strtoupper($value->durasi)}} Menit</td>
                                            <td>{{strtoupper($value->jumlah_soal)}} Soal</td>
                                            <td>
                                              <a href="{{route('lihat-nilai',['id' => $value->id_ujian])}}"><button type="button" class="btn btn-warning waves-effect waves-light"  >Lhat Nilai</button></a>
                                          </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          @foreach ($ujian as $key => $value)
                          <div id="modal-edit-{{$value->id_ujian}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-edit-{{$value->id_ujian}}" aria-hidden="true" style="display: none">
                            <div class="modal-dialog" style="width:30%">
                              <div class="modal-content">
                                <form class="" action="{{route('admin-mata-pelajaran-update', ['id' => $value->id_ujian])}}" method="post">
                                  {{ csrf_field() }}
                                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                  <h4 class="modal-title" id="modal-delete-mapel-">Edit Mata Pelajaran</h4></div>
                                  <div class="modal-body">
                                    <div class="panel-body ">
                                      <div class="input-group">
                                        <span class="input-group-addon"><i class="ion-android-book"></i></span>
                                        <input type="text" class="form-control" id="nama_matpel" name="nama_matpel" placeholder="Nama Mata Pelajaran " value="{{title_case($value->nama_matpel)}}">
                                      </div>
                                      <div class="jurusan" style="margin-bottom:10px;margin-top:10px;">
                                        <span class="input-group-addon"><label>Jurusan :</label>
                                          <label class="radio-inline"><input type="radio" name="jurusan" value="ipa" @if ($value->jurusan == 'ipa') checked @endif>IPA</label>
                                          <label class="radio-inline"><input type="radio" name="jurusan" value="ips" @if ($value->jurusan == 'ips') checked @endif>IPS</label>
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light"><span class="glyphicon glyphicon-floppy-disk"></span> Save changes</button>
                                  </div>
                                  </form>
                                  </div>
                                </div>
                              </div>
                              @endforeach
                              @foreach ($ujian as $key => $value)
                              <div id="modal-delete-{{$value->id_ujian}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-delete-{{$value->id_ujian}}" aria-hidden="true" style="display: none">
                                <div class="modal-dialog" style="width:30%">
                                  <div class="modal-content">
                                    <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                      <h4 class="modal-title" id="modal-delete-mapel-">Anda Yakin ?</h4></div>
                                      <div class="modal-body">
                                      Anda Akan Menghapus Permanen Data "<b>{{title_case($value->nama_matpel)}} - {{strtoupper($value->jurusan)}}</b>" ?
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> <a href="{{route('admin-mata-pelajaran-update', ['id'=>$value->id_ujian])}}"> <button type="button" class="btn btn-primary waves-effect waves-light"><span class="glyphicon glyphicon-floppy-disk"></span> Delete</button></a></div>
                                      </div>
                                    </div>
                                  </div>
                                @endforeach
                        </div>
                        <!-- <div class="col-md-4">
                <div class="panel text-center">
                  <div class="panel-heading">
                    <h4 class="panel-title text-muted font-light">Tambah Mata Pelajaran</h4></div>

                  <div class="panel-body p-t-10">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="ion-android-book"></i></span>
                      <form class="" action="{{route('mata-pelajaran-store')}}" method="post">
                        {{ csrf_field() }}
                    <input type="text" class="form-control" id="nama_matpel"  name="nama_matpel" placeholder="Nama Mata Pelajaran*" value="{{old('nama_matpel')}}">
                    </div>
                    <div class="jurusan" style="margin-bottom:10px;margin-top:10px;">
               <span class="input-group-addon"><label>Jurusan :</label>
               &nbsp; &nbsp;
               <label class="radio-inline"><input type="radio" name="jurusan" value="ipa">IPA</label>
               <label class="radio-inline"><input type="radio" name="jurusan" value="ips">IPS</label>
             </span>
                </div>
                <button type="submit" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-floppy-disk"></span> Save Data</button>
                </form>
                  </div>
                </div>
              </div>
                      </div> -->

          </div>
        </div>
      </div>
    </div>

@endsection
