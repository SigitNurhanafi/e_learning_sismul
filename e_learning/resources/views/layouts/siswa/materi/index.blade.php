@extends('layouts.siswa.master') @section('title', 'Materi') @section('content')
<div class="content">
  <div class="">
    <div class="page-header-title">
      <center>
        <h4 class="page-title">Materi Siswa {{$siswa->jurusan}}</h4></center>
    </div>

  </div>
  <div class="page-content-wrapper ">
    <div class="container">
      <div class="row">
        @if (session('msg'))
        <br>
        <div class="alert alert-warning alert-dismissible fade in">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">Ã—</span></button>
          <p>{!!session('msg')!!} </p>
        </div>
        @endif @if (count($errors) > 0)
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li> {{$error}} </li>
            @endforeach
          </ul>
        </div>
        @endif
        <div class="row">
          <div class="col-md-offset-1 col-md-10">
            <div class="panel panel-primary">
              <div class="panel-body">
                <h4 class="m-b-30 m-t-0"></h4>
                <div class="row">
                  <div class="col-xs-12">
                    <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Nama Materi</th>
                            <th>Mata Pelajaran</th>
                            <th>Action </th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($materi as $key => $value) {{-- <pre>{{ $value->id_materi }} {{ $value->nama_materi }} +++ {{$value->matpel}}</pre> --}} @if ($siswa->jurusan == $value->matpel['jurusan'])
                          <tr>
                            <td>{{$value->nama_materi }}</td>
                            <td>{{$value->matpel['nama_matpel']}}</td>
                            {{--
                            <td>{{ $value->nama_materi }}</td> --}}
                            <td>
                              <button type="button" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal-view-{{$value->id_materi}}">Lihat Materi</button>
                              <a href="{{route('materi-download', ['id' => $value->id_materi])}}">
                                <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="modal">Unduh</button>
                              </a>
                            </td>
                          </tr>
                          @endif
                        @endforeach
                        @foreach ($materi as $key => $value)
                           @if ($siswa->jurusan == $value->matpel['jurusan'])
                          <div id="modal-view-{{$value->id_materi}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" style="display: none">
                            <div class="modal-dialog" style="width:68%">
                              <div class="modal-content">
                                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Ã—</button>
                                  <div class="modal-body">
                                    @if ( in_array(explode('.', $value->file)[1],$video_format))
                                    <video id="id-video-{{$value->id_materi}}" width="100%" controls>
                                      <source src="{{asset('storage/materi')}}/{{$value->file}}" type="video/mp4">
                                        <script>
                                        var vid_{{$value->id_materi}} = document.getElementById("id-video-{{$value->id_materi}}");
                                        function pauseVid{{$value->id_materi}}() {
                                          vid_{{$value->id_materi}}.pause();
                                                                // console.log('dipencet');
                                        }
                                        </script>
                                                          Your browser does not support HTML5 video.
                                                        </video>
                                                  @elseif (in_array(explode('.', $value->file)[1],$document_format))
                                    <a href="{{route('materi-download', ['id' => $value->id_materi])}}">
                                                        <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="modal">Unduh</button>
                                                      </a>
                                                    @endif
                                    <hr>
                                    <div class="panel panel-fill panel-primary">
                                      <div class="panel-heading">
                                        <h3 class="panel-title">Penjelasan Materi</h3>
                                      </div>
                                      <div class="panel-body">
                                        {{$value->deskripsi}}
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default waves-effect" onclick="pauseVid{{$value->id_materi}}()" data-dismiss="modal">Close</button></div>
                                  </div>
                                </div>
                              </div>
                        </tbody>
                      </table>
                      </div>
                      </div>
                      @endif @endforeach
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
          </div>
        </div>
      </div>
    </div>

    @endsection
