@extends('layouts.guru.master')
@section('title', 'Olah Materi')
@section('content')
  <div class="content">
      <div class="">
        <div class="page-header-title">
          <h4 class="page-title">Olah Data Materi</h4>
        </div>

      </div>
      <div class="page-content-wrapper ">
        <div class="container">
          <div class="row">
                    @if (session('msg'))
                      <br>
                      <div class="alert alert-warning alert-dismissible fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"> </span></button>  <p>{!!session('msg')!!} </p></div>
                    @endif
                    @if (count($errors) > 0)
                      <div class="alert alert-danger">
                        <ul>
                          @foreach ($errors->all() as $error)
                            <li> {{$error}} </li>
                          @endforeach
                        </ul>
                      </div>
                    @endif
            <div class="row">

              <div class="col-md-3">

                <form action="{{route('upload-materi')}}" method="post" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <div class="form-group">
                    <label>Nama Materi</label>
                    <input class="form-control" type="text" placeholder="masukkan teks disini" name="nama_materi" style="margin-bottom:5px;">
                  </div>
                  <label for=""> Deskripsi</label>
                  <textarea  placeholder="masukkan teks disini" name="deskripsi" class="form-control" rows="5"></textarea>
                  <div class="form-group">
                    {{-- <label>Materi Mata Pelajaran</label>
                    <select  name="id_matpel" class="form-control">
                      <option>- Pilih Mata Pelajaran -</option>
                      @foreach ($mata_pelajaran as $key => $value)
                        <option  value="{{$value->id_matpel}}">{{$value->nama_matpel}} ({{$value->jurusan}})</option>
                      @endforeach
                    </select> --}}

                    <label class="btn btn-default btn-file btn-block" style="margin-top:10px;">
                        Browse <input class="form-control " name="materi" type="file" style="display: none;">
                    </label>
                    <div class="form-group" style="margin-top:10px;">
                      <button type="submit" class="btn btn-primary btn-block" >Upload</button>
                    </div>
                  </div>
                </form>
              </div>
              <div class="col-md-9">
                <div class="panel panel-primary">
                            <div class="panel-body">
                              <h4 class="m-b-30 m-t-0"></h4>
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="table-responsive">
                                    <table class="table">
                                      <thead>
                                        <tr>
                                          <th>Kode Materi</th>
                                          <th>Nama Materi</th>
                                          <th>Mata Pelajaran</th>
                                          <th>Jurusan </th>
                                          <th>Action </th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach ($materi as $key => $value)
                                          {{-- <pre>{{ $value->id_materi }} {{ $value->nama_materi }} +++ {{$value->matpel}}</pre> --}}
                                          @if ($guru->nama_matpel == $value->matpel['nama_matpel'])
                                        <tr>
                                            <td>{{$value->id_materi}}</td>
                                            <td>{{$value->nama_materi }}</td>
                                            <td>{{$value->matpel['nama_matpel']}}</td>
                                            <td>{{$value->matpel['jurusan']}}</td>
                                            {{-- <td>{{ $value->nama_materi }}</td> --}}
                                        <td>
                                              <button type="button" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal-edit-{{$value->id_materi}}">Edit</button>
                                              <button type="button" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal-view-{{$value->id_materi}}" >Lihat Materi</button>
                                               <a href="{{route('materi-download', ['id' => $value->id_materi])}}">
                                                 <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#modal-delete-1">Unduh</button>
                                               </a>
                                               <a href="{{route('materi-delete', ['id' => $value->id_materi])}}">
                                                 <button type="button" class="btn btn-danger waves-effect waves-light" data-toggle="modal" data-target="#modal-delete-1">Delete</button>
                                               </a>

                                            </td>
                                        </tr>
                                          @endif
                                        @endforeach

                                        @foreach ($materi as $key => $value)
                                        @if ($guru->nama_matpel == $value->matpel['nama_matpel'])
                                        <div id="modal-view-{{$value->id_materi}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" style="display: none">
                                          <div class="modal-dialog" style="width:70%">
                                            <div class="modal-content">
                                              <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> </button>
                                                <div class="modal-body">
                                                  @if ( in_array(explode('.', $value->file)[1],$video_format))
                                                    <video  id="id-video-{{$value->id_materi}}" width="100%" controls>
                                                      <source src="{{asset('storage/materi')}}/{{$value->file}}" type="video/mp4">
                                                        <script>
                                                            var vid_{{$value->id_materi}} = document.getElementById("id-video-{{$value->id_materi}}");
                                                            function pauseVid{{$value->id_materi}}() {
                                                                vid_{{$value->id_materi}}.pause();
                                                                console.log('dipencet');
                                                            }
                                                        </script>
                                                          Your browser does not support HTML5 video.
                                                    </video>
                                                  @elseif (in_array(explode('.', $value->file)[1],$document_format))
                                                    <a href="{{route('materi-download', ['id' => $value->id_materi])}}">
                                                      <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="modal">Unduh</button>
                                                    </a>
                                                  @endif
                                                  <hr>
                                                  <div class="panel panel-fill panel-primary">
                                                    <div class="panel-heading">
                                                      <h3 class="panel-title">Penjelasan Materi</h3>
                                                    </div>
                                                    <div class="panel-body">
                                                    {{$value->deskripsi}}
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="modal-footer">
                                                  <button type="button" class="btn btn-default waves-effect" onclick="pauseVid{{$value->id_materi}}()" data-dismiss="modal">Close</button></div>
                                                </div>
                                              </div>
                                            </div>
                                      </tbody>
                                    </table>
                                  </div>

                                  <div id="modal-edit-{{$value->id_materi}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" style="display: none">
                                    <div class="modal-dialog" style="width:70%">
                                      <div class="modal-content">
                                        <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> </button>
                                          <div class="modal-body">
                                            <td>{{$value->id_materi}}</td>
                                            <td>{{$value->nama_materi }}</td>
                                            <td>{{$value->matpel['nama_matpel']}}</td>
                                            <td>{{$value->matpel['jurusan']}}</td>
                                          </div>
                                          <form action="{{route('materi-update', ['id'=>$value->id_materi])}}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="id_materi" value="{{$value->id_materi}}">
                                            <div class="form-group">
                                              <label>Nama Materi</label>
                                              <input class="form-control" value="{{$value->nama_materi }}" type="text" placeholder="masukkan teks disini" name="nama_materi" style="margin-bottom:5px;">
                                            </div>
                                            <label for=""> Deskripsi</label>
                                            <textarea placeholder="masukkan teks disini" name="deskripsi" class="form-control" rows="5">{{$value->deskripsi}}</textarea>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary btn-block">Update</button>
                                              </div>
                                            </div>
                                          </form>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" onclick="pauseVid{{$value->id_materi}}()" data-dismiss="modal">Close</button></div>
                                          </div>
                                        </div>
                                      </div>
                                </tbody>
                              </table>
                            </div>
                                  @endif

                                  @endforeach
                                </div>
                              </div>
                            </div>
                          </div>
              </div>
            </div>

          </div>
          <div class="row">
          </div>
        </div>
      </div>
    </div>

@endsection
