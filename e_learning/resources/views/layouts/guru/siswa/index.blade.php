@extends('layouts.guru.master')
@section('title', 'Olah Nilai')
@section('content')
  <div class="content">
      <div class="">
        <div class="page-header-title">
          <h4 class="page-title">Olah Data Nilai Siswa</h4></div>
      </div>
      <div class="page-content-wrapper ">
        <div class="container">
          <div class="row">

            <div class="row">
                        <div class="col-md-12">
                          <div class="panel panel-primary">
                            <div class="panel-body">
                              <h4 class="m-b-30 m-t-0"></h4>
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="table-responsive">
                                    <table class="table">
                                      <thead>
                                        <tr>
                                          <th>Nama</th>
                                          <th>Aksi</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach ($ujian as $key => $value)
                                          <tr>
                                            <td>{{$value->name}}</td>
                                            <td><a href="{{route('nilai-siswa',['id' => $value->id])}}"><button type="button" class="btn btn-warning waves-effect waves-light" data-toggle="modal"  >Lihat Siswa</button></a></td>
                                          </tr>
                                        @endforeach
                                        <div id="modal-edit-" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-edit-" aria-hidden="true" style="display: none">
                                          <div class="modal-dialog" style="width:30%">
                                            <div class="modal-content">
                                              <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="modal-delete-mapel-">Edit Mata Pelajaran</h4></div>
                                                <div class="modal-body">
                                                  <div class="panel-body ">
                                                    <div class="input-group">
                                                      <span class="input-group-addon"><i class="ion-android-book"></i></span>
                                                      <input type="text" class="form-control" id="nama_matpel" name="nama_matpel" placeholder="Nama Mata Pelajaran " value="">
                                                    </div>
                                                    <div class="jurusan" style="margin-bottom:10px;margin-top:10px;">
                                                      <span class="input-group-addon"><label>Jurusan :</label>
                                                        <label class="radio-inline"><input type="radio" name="jurusan" value="ipa">IPA</label>
                                                        <label class="radio-inline"><input type="radio" name="jurusan" value="ips">IPS</label>
                                                      </span>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="modal-footer">
                                                  <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> <button type="button" class="btn btn-primary waves-effect waves-light"><span class="glyphicon glyphicon-floppy-disk"></span> Save changes</button></div>
                                                </div>
                                              </div>
                                            </div>

                                            <div id="modal-edit-x" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-edit-x" aria-hidden="true" style="display: none">
                                              <div class="modal-dialog" style="width:30%">
                                                <div class="modal-content">
                                                  <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title" id="modal-delete-mapel-">Anda Yakin ?</h4></div>
                                                    <div class="modal-body">
                                                    Anda Akan Menghapus Permanen Data "" ?
                                                    </div>
                                                    <div class="modal-footer">
                                                      <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> <button type="button" class="btn btn-primary waves-effect waves-light"><span class="glyphicon glyphicon-floppy-disk"></span> Delete</button></div>
                                                    </div>
                                                  </div>
                                                </div>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <!--  -->

                      </div>

          </div>
        </div>
      </div>
    </div>

@endsection
