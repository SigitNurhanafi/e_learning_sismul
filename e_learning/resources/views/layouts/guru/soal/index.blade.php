@extends('layouts.guru.master')
@section('title', 'Olah Soal')
@section('content')
  <div class="content">
      <div class="">
        <div class="page-header-title">
          <h4 class="page-title">Olah Data Soal {{$nama_matpel}}</h4>
          <td><button type="button" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#tambah_soal" >Tambah</button>
        </div>

      </div>
      <div class="page-content-wrapper ">
        <div class="container">
          <div class="row">
                    @if (session('msg'))
                      <br>
                      <div class="alert alert-warning alert-dismissible fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>  <p>{!!session('msg')!!} </p></div>
                    @endif
                    @if (count($errors) > 0)
                      <div class="alert alert-danger">
                        <ul>
                          @foreach ($errors->all() as $error)
                            <li> {{$error}} </li>
                          @endforeach
                        </ul>
                      </div>
                    @endif
            <div class="row">
                        <div class="col-md-12">
                          <?php $a = 1 ?>
                          @foreach ($mata_pelajaran as $key => $value)
                          <div class="panel panel-primary">
                            <div class="panel-body">
                              <h4 class="m-b-30 m-t-0"></h4>
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="table-responsive">
                                    @if ($value->file != NULL )
                                    <center><img width="20%" src="{{asset('/storage/upload/soal//')}}/{{$value->file }}" alt="file {{$value->pertanyaan}}"></center>
                                    <br>
                                    @endif
                                    <p>{{$value->pertanyaan}}</p>
                                    <p>A. {{$value->jawaban_a}}</p>
                                    <p>B. {{$value->jawaban_b}}</p>
                                    <p>C. {{$value->jawaban_c}}</p>
                                    <p>D. {{$value->jawaban_d}}</p>
                                    <p>E. {{$value->jawaban_e}}</p>
                                    <p>Kunci Jawaban : {{$value->kunci_jawaban}}</p>
                                    <tr>
                                      <td>{{title_case($value->nama_mata_pelajaran)}}</td>
                                      <td>{{strtoupper($value->jurusan)}}</td>
                                      <td><button type="button" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal-edit-{{$value->id}}" >Edit</button>
                                    </tr>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <?php echo str_replace('/?', '?', $mata_pelajaran->render()); ?>
                          @endforeach
                          <!-- Pop Up Tambah Soal -->
                          <div id="tambah_soal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-edit-{{$value->id_mata_pelajaran}}" aria-hidden="true" style="display: none">
                            <div class="modal-dialog" style="width:30%">
                              <div class="modal-content">
                                <form class="" action="{{route('mata-pelajaran-store')}}" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                  <h4 class="modal-title" id="modal-delete-mapel-">Tambah Soal</h4></div>
                                  <div class="modal-body">
                                    <div class="panel-body ">
                                      <div class="input-group" style="margin-bottom:10px;">
                                        <span class="input-group-addon"><i class="ion-android-book"></i></span>
                                        <textarea id="pertanyaan" name="pertanyaan" rows="8" cols="33"></textarea>
                                      </div>
                                      <div class="input-group" style="margin-bottom:10px;">
                                        <span class="input-group-addon"><i class="ion-android-book"></i></span>
                                        <input type="text" class="form-control" id="nama_matpel" name="jawaban_a" placeholder="Jawaban A" >
                                      </div>
                                      <div class="input-group" style="margin-bottom:10px;">
                                        <span class="input-group-addon"><i class="ion-android-book"></i></span>
                                        <input type="text" class="form-control" id="nama_matpel" name="jawaban_b" placeholder="Jawaban B" >
                                      </div>
                                      <div class="input-group" style="margin-bottom:10px;">
                                        <span class="input-group-addon"><i class="ion-android-book"></i></span>
                                        <input type="text" class="form-control" id="nama_matpel" name="jawaban_c" placeholder="Jawaban C" >
                                      </div>
                                      <div class="input-group" style="margin-bottom:10px;">
                                        <span class="input-group-addon"><i class="ion-android-book"></i></span>
                                        <input type="text" class="form-control" id="nama_matpel" name="jawaban_d" placeholder="Jawaban D" >
                                      </div>
                                      <div class="input-group" style="margin-bottom:10px;">
                                        <span class="input-group-addon"><i class="ion-android-book"></i></span>
                                        <input type="text" class="form-control" id="nama_matpel" name="jawaban_e" placeholder="Jawaban E" >
                                      </div>
                                      <div class="input-group" style="margin-bottom:10px;">
                                        <span class="input-group-addon"><i class="ion-android-book"></i></span>
                                        <select class="form-control" name="kunci_jawaban">
                                          <option value="">-- Pilih Kunci Jawaban --</option>
                                          <option value="A">A</option>
                                          <option value="B">B</option>
                                          <option value="C">C</option>
                                          <option value="D">D</option>
                                          <option value="E">E</option>
                                        </select>
                                      </div>
                                      <div class="input-group">
                                        {{-- <span class="input-group-addon"></span> --}}
                                        <input type="file" name="img_soal" accept="image/*">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light"><span class="glyphicon glyphicon-floppy-disk"></span> Save changes</button>
                                  </div>
                                  </form>
                                  </div>
                                </div>
                              </div>
                            <!-- Akhir Pop Up Tambah Soal -->
                            <!-- Awal POp untuk Edit -->
                          @foreach ($mata_pelajaran as $key => $value)
                          <div id="modal-edit-{{$value->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-edit-{{$value->id}}" aria-hidden="true" style="display: none">
                            <div class="modal-dialog" style="width:30%">
                              <div class="modal-content">
                                <form class="" action="{{route('soal-update', ['id' => $value->id])}}" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                  <h4 class="modal-title" id="modal-delete-mapel-">Edit Soal</h4></div>
                                  <div class="modal-body">
                                    <div class="panel-body ">
                                      <div class="input-group" style="margin-bottom:10px;">
                                        <span class="input-group-addon"><i class="ion-android-book"></i></span>
                                        <textarea id="pertanyaan" name="pertanyaan" rows="8" cols="33" >{{$value->pertanyaan}}</textarea>
                                      </div>
                                      <div class="input-group" style="margin-bottom:10px;">
                                        <span class="input-group-addon"><i class="ion-android-book"></i></span>
                                        <input type="text" class="form-control" id="nama_matpel" name="jawaban_a" placeholder="Jawaban A" value="{{$value->jawaban_a}}">
                                      </div>
                                      <div class="input-group" style="margin-bottom:10px;">
                                        <span class="input-group-addon"><i class="ion-android-book"></i></span>
                                        <input type="text" class="form-control" id="nama_matpel" name="jawaban_b" placeholder="Jawaban B" value="{{$value->jawaban_b}}">
                                      </div>
                                      <div class="input-group" style="margin-bottom:10px;">
                                        <span class="input-group-addon"><i class="ion-android-book"></i></span>
                                        <input type="text" class="form-control" id="nama_matpel" name="jawaban_c" placeholder="Jawaban C" value="{{$value->jawaban_c}}">
                                      </div>
                                      <div class="input-group" style="margin-bottom:10px;">
                                        <span class="input-group-addon"><i class="ion-android-book"></i></span>
                                        <input type="text" class="form-control" id="nama_matpel" name="jawaban_d" placeholder="Jawaban D" value="{{$value->jawaban_d}}">
                                      </div>
                                      <div class="input-group" style="margin-bottom:10px;">
                                        <span class="input-group-addon"><i class="ion-android-book"></i></span>
                                        <input type="text" class="form-control" id="nama_matpel" name="jawaban_e" placeholder="Jawaban E" value="{{$value->jawaban_e}}">
                                      </div>
                                      <div class="input-group" style="margin-bottom:10px;">
                                        <span class="input-group-addon"><i class="ion-android-book"></i></span>
                                        <select class="form-control" name="kunci_jawaban">
                                          <option value="{{$value->kunci_jawaban}}">{{$value->kunci_jawaban}}</option>
                                          <option value="">-- Pilih Kunci Jawaban --</option>
                                          <option value="A">A</option>
                                          <option value="B">B</option>
                                          <option value="C">C</option>
                                          <option value="D">D</option>
                                          <option value="E">E</option>
                                        </select>
                                      </div>
                                      @if ($value->file != NULL )
                                      <center><img width="75%" src="{{asset('/storage/upload/soal')}}/{{$value->file }}" alt="file {{$value->pertanyaan}}"></center>
                                      <br>
                                      @endif
                                      <div class="input-group">
                                        {{-- <span class="input-group-addon"></span> --}}
                                        <input type="file" name="img_soal" accept="image/*">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light"><span class="glyphicon glyphicon-floppy-disk"></span> Save changes</button>
                                  </div>
                                  </form>
                                  </div>
                                </div>
                              </div>
                              @endforeach
                              <!-- Akhir Pop Up Edit -->
                        </div>

                      </div>

          </div>
        </div>
      </div>
    </div>

@endsection
