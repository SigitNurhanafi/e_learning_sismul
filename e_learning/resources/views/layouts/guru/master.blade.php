<!DOCTYPE html>
<html>
<!-- Mirrored from themesdesign.in/appzia/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 18 Dec 2017 19:23:54 GMT -->

<head>
  <meta charset="utf-8" />
  <title>Admin - @yield('title')</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <meta content="Admin Dashboard" name="description" />
  <meta content="ThemeDesign" name="author" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <link rel="shortcut icon" href="/assets/images/favicon.ico">
  <link rel="stylesheet" href="/assets/plugins/morris/morris.css">
  <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="/assets/css/icons.css" rel="stylesheet" type="text/css">
  <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
</head>

<body class="fixed-left">
  <div id="wrapper">
    <div class="topbar">
      <div class="topbar-left">
        <div class="text-center"> <a href="/home" class="logo"><img src="/assets/images/logo.png"></a> <a href="/home" class="logo-sm"><img src="/assets/images/logo_sm.png"></a></div>
      </div>
      <div class="navbar navbar-default" role="navigation">
        <div class="container">
          <div class="">
            <div class="pull-left"> <button type="button" class="button-menu-mobile open-left waves-effect waves-light"> <i class="ion-navicon"></i> </button> <span class="clearfix"></span></div>
            {{-- <form class="navbar-form pull-left" role="search">
              <div class="form-group"> <input type="text" class="form-control search-bar" placeholder="Search..."></div> <button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button></form> --}}
            <ul class="nav navbar-nav navbar-right pull-right">
              {{-- <li class="dropdown hidden-xs"> <a href="#" data-target="#" class="dropdown-toggle waves-effect waves-light notification-icon-box" data-toggle="dropdown" aria-expanded="true"> <i class="fa fa-bell"></i> <span class="badge badge-xs badge-danger"></span> </a>
                <ul class="dropdown-menu dropdown-menu-lg noti-list-box">
                  <li class="text-center notifi-title">Notification <span class="badge badge-xs badge-success">3</span></li>
                  <li class="list-group">
                    <a href="javascript:void(0);" class="list-group-item">
                      <div class="media">
                        <div class="media-heading">Your order is placed</div>
                        <p class="m-0"> <small>Dummy text of the printing and typesetting industry.</small></p>
                      </div>
                    </a>
                    <a href="javascript:void(0);" class="list-group-item">
                      <div class="media">
                        <div class="media-body clearfix">
                          <div class="media-heading">New Message received</div>
                          <p class="m-0"> <small>You have 87 unread messages</small></p>
                        </div>
                      </div>
                    </a>
                    <a href="javascript:void(0);" class="list-group-item">
                      <div class="media">
                        <div class="media-body clearfix">
                          <div class="media-heading">Your item is shipped.</div>
                          <p class="m-0"> <small>It is a long established fact that a reader will</small></p>
                        </div>
                      </div>
                    </a> <a href="javascript:void(0);" class="list-group-item"> <small class="text-primary">See all notifications</small> </a></li>
                </ul>
              </li> --}}
              <li class="hidden-xs"> <a href="#" id="btn-fullscreen" class="waves-effect waves-light notification-icon-box"><i class="mdi mdi-fullscreen"></i></a></li>
              <li class="dropdown"> <a href="#" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true"> <img src="/assets/images/users/avatar-1.jpg" alt="user-img" class="img-circle"> </a>
                <ul class="dropdown-menu">
                  <li><a href="javascript:void(0)"> Profile</a></li>
                  <li><a href="javascript:void(0)"> Settings </a></li>
                  {{-- <li><a href="javascript:void(0)"> Lock screen</a></li> --}}
                  <li class="divider"></li>
                  <li><a href="/logout"> Logout</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

  @include('layouts.guru.menu.side_menu')

    <div class="content-page">
      @yield('content')
    </div>
  </div>
  <script src="/assets/js/jquery.min.js"></script>
  <script src="/assets/js/bootstrap.min.js"></script>
  <script src="/assets/js/modernizr.min.js"></script>
  <script src="/assets/js/detect.js"></script>
  <script src="/assets/js/fastclick.js"></script>
  <script src="/assets/js/jquery.slimscroll.js"></script>
  <script src="/assets/js/jquery.blockUI.js"></script>
  <script src="/assets/js/waves.js"></script>
  <script src="/assets/js/wow.min.js"></script>
  <script src="/assets/js/jquery.nicescroll.js"></script>
  <script src="/assets/js/jquery.scrollTo.min.js"></script>
  <script src="/assets/plugins/morris/morris.min.js"></script>
  <script src="/assets/plugins/raphael/raphael-min.js"></script>
  <script src="/assets/pages/dashborad.js"></script>
  <script src="/assets/js/app.js"></script>
</body>
<!-- Mirrored from themesdesign.in/appzia/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 18 Dec 2017 19:23:57 GMT -->

</html>
