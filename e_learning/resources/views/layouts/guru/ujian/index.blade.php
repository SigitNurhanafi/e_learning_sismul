@extends('layouts.guru.master')
@section('title', 'Olah Ujian')
@section('content')
  <div class="content">
      <div class="">
        <div class="page-header-title">
          <h4 class="page-title">Olah Data Mata Pelajaran</h4>
        </div>

      </div>
      <div class="page-content-wrapper ">
        <div class="container">
          <div class="row">
                    @if (session('msg'))
                      <br>
                      <div class="alert alert-warning alert-dismissible fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>  <p>{!!session('msg')!!} </p></div>
                    @endif
                    @if (count($errors) > 0)
                      <div class="alert alert-danger">
                        <ul>
                          @foreach ($errors->all() as $error)
                            <li> {{$error}} </li>
                          @endforeach
                        </ul>
                      </div>
                    @endif
            <div class="row">
                        <div class="col-md-8">
                          <div class="panel panel-primary">
                            <div class="panel-body">
                              <h4 class="m-b-30 m-t-0"></h4>
                              <div class="row">
                                <div class="col-xs-12">
                                  <div class="table-responsive">
                                    <table class="table">
                                      <thead>
                                        <tr>
                                          <th>Nama Ujian</th>
                                          <th>Durasi</th>
                                          <th>Jumlah Soal</th>
                                          <th>Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach ($ujian as $key => $value)
                                          <tr>
                                            <td>{{title_case($value->nama_ujian)}}</td>
                                            <td>{{strtoupper($value->durasi)}}</td>
                                            <td>{{strtoupper($value->jumlah_soal)}}</td>
                                            <td><button type="button" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#modal-edit-{{$value->id_ujian}}" >Edit</button> <button type="button" class="btn btn-danger waves-effect waves-light" data-toggle="modal" data-target="#modal-delete-{{$value->id_ujian}}">Delete</button></td>
                                          </tr>
                                          <?php $id_matpel = $value->id_matpel; ?>
                                        @endforeach
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          @foreach ($ujian as $key => $value)
                          <div id="modal-edit-{{$value->id_ujian}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-edit-{{$value->id_ujian}}" aria-hidden="true" style="display: none">
                            <div class="modal-dialog" style="width:30%">
                              <div class="modal-content">
                                <form class="" action="{{route('ujian-update', ['id' => $value->id_ujian])}}" method="post">
                                  {{ csrf_field() }}
                                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                  <h4 class="modal-title" id="modal-delete-mapel-">Edit Ujian</h4></div>
                                  <div class="modal-body">
                                    <div class="panel-body ">
                                      <div class="input-group">
                                        <span class="input-group-addon"><i class="ion-android-book"></i></span>
                                        <input type="text" class="form-control" id="nama_ujian" name="nama_ujian" placeholder="Nama Ujian " value="{{title_case($value->nama_ujian)}}">
                                      </div>
                                      <div class="input-group">
                                        <span class="input-group-addon"><i class="ion-android-book"></i></span>
                                        <input type="text" class="form-control" id="durasi" name="durasi" placeholder="Durasi " value="{{title_case($value->durasi)}}">
                                      </div>
                                      <div class="input-group">
                                        <span class="input-group-addon"><i class="ion-android-book"></i></span>
                                        <input type="text" class="form-control" id="jumlah_soal" name="jumlah_soal" placeholder="Jumlah Soal " value="{{title_case($value->jumlah_soal)}}">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light"><span class="glyphicon glyphicon-floppy-disk"></span> Save changes</button>
                                  </div>
                                  </form>
                                  </div>
                                </div>
                              </div>
                              @endforeach
                              @foreach ($ujian as $key => $value)
                              <div id="modal-delete-{{$value->id_ujian}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-delete-{{$value->id_ujian}}" aria-hidden="true" style="display: none">
                                <div class="modal-dialog" style="width:30%">
                                  <div class="modal-content">
                                    <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                      <h4 class="modal-title" id="modal-delete-mapel-">Anda Yakin ?</h4></div>
                                      <div class="modal-body">
                                      Anda Akan Menghapus Permanen Data "<b>{{title_case($value->nama_ujian)}}</b>" ?
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> <a href="{{route('ujian-delete', ['id'=>$value->id_ujian])}}"> <button type="button" class="btn btn-primary waves-effect waves-light"><span class="glyphicon glyphicon-floppy-disk"></span> Delete</button></a></div>
                                      </div>
                                    </div>
                                  </div>
                                @endforeach
                        </div>
                        <div class="col-md-4">
                <div class="panel text-center">
                  <div class="panel-heading">
                    <h4 class="panel-title text-muted font-light">Tambah Ujian</h4></div>

                  <div class="panel-body p-t-10">
                    <form class="" action="{{route('ujian-store')}}" method="post">
                      {{ csrf_field() }}
                      <input type="hidden" class="form-control" id="id_matpel"  name="id_matpel" value="{{$id_matpel}}">
                    <div class="input-group" style="margin-bottom:10px;">
                      <span class="input-group-addon"><i class="ion-android-book"></i></span>
                      <input type="text" class="form-control" id="nama_ujian"  name="nama_ujian" placeholder="Nama Ujian*" value="{{old('nama_ujian')}}">
                    </div>
                    <div class="input-group" style="margin-bottom:10px;">
                      <span class="input-group-addon"><i class="ion-android-book"></i></span>

                      <input type="number" class="form-control" id="durasi"  name="durasi" placeholder="Durasi*" value="{{old('durasi')}}">
                    </div>
                    <div class="input-group" style="margin-bottom:10px;">
                      <span class="input-group-addon"><i class="ion-android-book"></i></span>

                      <input type="number" class="form-control" id="jumlah_soal"  name="jumlah_soal" placeholder="Jumlah Soal*" value="{{old('nama_matpel')}}">
                    </div>
                <button type="submit" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-floppy-disk"></span> Save Data</button>
                </form>
                  </div>
                </div>
              </div>
                      </div>

          </div>
        </div>
      </div>
    </div>

@endsection
