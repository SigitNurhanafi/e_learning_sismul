$(document).ready(function() {

});function generate_token()
{
     $.ajaxSetup({
          headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
     });
}

function login()
{
     generate_token();
     var formData = $("#form_login").serialize();
     var url = "log";
     $.ajax({
          type: "POST",
          url: url,
          data: formData,
          beforeSend  : function(){
              $("#loading").show();
            },
          success: function(data){
               console.log(data);
               $("#loading").hide();
               switch (data) {
                    case "1":
                         window.location.href = "admin";
                    break;
                    case "2":
                         window.location.href = "member";
                    break;
                    case "3":
                         $("#pesan_error").removeClass('hidden');
                         $("#pesan_error span").html("Akun Anda Belum di Aktivasi");
                    break;
                    case "4":
                         swal({
                              title: "ERROR",
                              text: "Username atau Password Salah",
                              type: "error",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         });
                    break;
               }
          }
     });
}
// $("#button_log").click(function(event) {
//      });
