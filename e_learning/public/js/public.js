//Disable Enter Key
$("input").bind("keypress", function(e) {
      if (e.keyCode == 13) {
          return false;
      }
});

function generate_token(){
     $.ajaxSetup({
          headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
     });
}

function ajax_operation(tipe,url,content){
    $.ajax({
      type: tipe,
      url: url,
      beforeSend  : function(){
          $("#loading").show();
        },
      success: function(data){
        $("#loading").hide();
        if(content!=""){
          $(content).html(data);
        }
      }
    });
 }

 function show_pass(){
      //Tampil Password Jika Chechkbok pada form di click
      var checkbox = $('#tampil-pass');
      var password = $('#password');
      var icon = $('#icon-change');
      if(checkbox[0].checked == true){
           password[0].type = 'text';
           icon[0].className = 'glyphicon glyphicon-eye-open';
      }else{
           password[0].type = 'password';
           icon[0].className = 'glyphicon glyphicon-eye-close';
      }
 }

function search_data(obj,table){
   var $rows = $('#'+table+' tbody tr');
   var val = $.trim($(obj).val()).replace(/ +/g, ' ').toLowerCase();

   $rows.show().filter(function() {
       var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
       return !~text.indexOf(val);
   }).hide();
 }


 function logout()
 {
      // url = "/WO/logout";
      $.ajax({
           type: 'GET',
           url: '/WO/logout',
           beforeSend  : function()
           {
               $("#loading").show();
           },
           success: function()
           {
               location.reload();
           }
       });
 }
