
$(document).ready(function(){
     // Initialize Tooltip
     $('[data-toggle="tooltip"]').tooltip();

     // Add smooth scrolling to all links in navbar + footer link
     $(".navbar a, footer a[href='#myPage']").on('click', function(event){

          // Make sure this.hash has a value before overriding default behavior
          if (this.hash !== "") {

               // Prevent default anchor click behavior
               event.preventDefault();

               // Store hash
               var hash = this.hash;

               // Using jQuery's animate() method to add smooth page scroll
               // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
               $('html, body').animate({
               scrollTop: $(hash).offset().top
               }, 900, function(){

               // Add hash (#) to URL when done scrolling (default click behavior)
               window.location.hash = hash;
               });
          } // End if
     });

     function generate_token(){
          $.ajaxSetup({
               headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               }
          });
     }

     //Handle Form-Login
     $("#button_login").click(function(event){

          generate_token();
          var formData = $("#form_login").serialize();
          var url = "log";
          $.ajax({
               type: "POST",
               url: url,
               data: formData,
               beforeSend  : function(){
                   $("#loading").show();
               },
               success: function(data){
                    console.log(data);
                    $("#loading").hide();
                    switch (data) {
                         case "1":
                              window.location.href = "admin";
                         break;
                         case "2":
                              window.location.href = "trainer";
                         break;
                         case "3":
                         swal({
                              title: "ERROR",
                              text: "Username atau Password Salah",
                              type: "error",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         });
                         break;
                    }
               }
          });
     });

     //Close Modal Login dan Lupa Password Jika Link Daftar di Klik
     $('.daftar').on('click', function(event){
          $('#loginModal').modal('hide');
          $('#forgetpass').modal('hide');
     });

     //Handle Form-Lupa Password
     $("#button_forgetpass").click(function(event) {
          generate_token();
          var formData = $("#form_forgetpass").serialize();
          var url = "forgetPass";
          $("#pesan_forpass").addClass('hidden');
          $.ajax({
               type: "POST",
               url: url,
               data: formData,
               beforeSend  : function(){
                   $("#loading").show();
               },
               success: function(data){
                    $("#loading").hide();
                    if(data == 1){
                         swal({
                              title: "SUCCESS",
                              text: "Reset Password Berhasil, Silakan Cek Email Anda",
                              type: "success",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         });
                    }else{
                         $("#pesan_forpass").removeClass('hidden');
                         $("#pesan_forpass span").html("Alamat Email Yang Anda Masukan Salah");
                    }
               }
          });
     });

     //Tampil Password Jika Chechkbok pada form di click
     var checkbox = $('#tampil-pass');
     var password = $('#password');
     var icon = $('#icon-change');
     checkbox.on('click',function(event) {
          if(checkbox[0].checked == true){
               password[0].type = 'text';
               icon[0].className = 'glyphicon glyphicon-eye-open';
          }else{
               password[0].type = 'password';
               icon[0].className = 'glyphicon glyphicon-eye-close';
          }
     });

     //Handle Form Registrasi
     $("#button_reg").click(function(event) {
          $("#pesan_nama").addClass("hidden");
          $("#pesan_notelp").addClass("hidden");
          $("#pesan_email").addClass("hidden");
          $("#pesan_alamat").addClass("hidden");
          $("#pesan_username").addClass("hidden");
          $("#pesan_password").addClass("hidden");
          $("#pesan_jk").addClass("hidden");
          generate_token();
          var formData = $("#form_registrasi").serialize();
          var url = "reg";
          $.ajax({
               type: "POST",
               url: url,
               data: formData,
               beforeSend  : function(){
                   $("#loading").show();
                 },
               success: function(data){
                 $("#loading").hide();
                 swal({
                      title: "SUCCESS",
                      text: "Registrasi Berhasil, Silakan Cek Email Anda",
                      type: "success",
                      confirmButtonColor: "#2b5dcd",
                      confirmButtonText: "OK",
                      closeOnConfirm: true
                 });
                 $("#form_registrasi").trigger('reset'); //Reset form
               },
               error: function(data){
                    $("#loading").hide();
                    var obj = jQuery.parseJSON(data.responseText);
                    $.each(obj,function(key, value) {
                         $("#pesan_"+key).removeClass("hidden");
                         $("#pesan_"+key+" span").html(value);
                    });
               }
          });
     });

     //Tampil Modal Jika URL Anchor Aktif
     if(window.location.href.indexOf('#loginModal') != -1) {
          $('#loginModal').modal('show');
     }else if(window.location.href.indexOf('#forgetpass') != -1){
          $('#forgetpass').modal('show');
     }

     //Hapus Anchor yang ada di url ketika tombol close di klik
     $('button.close').click(function(event) {
          history.pushState("", document.title, window.location.pathname);
     });

     //Ubah URL ketika link password pada modal di click
     $('#for_pass').click(function(event) {
          window.location.href = "http://localhost/WO/#forgetpass";
     });

     //Ubah URL ketika link login pada modal di click
     $('#login').click(function(event) {
          window.location.href = "http://localhost/WO/#loginModal";
     });

}) //End document
