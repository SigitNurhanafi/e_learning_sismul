$(document).ready(function(){
     // Initialize Tooltip
     $('[data-toggle="tooltip"]').tooltip();

     // Add smooth scrolling to all links in navbar + footer link
     $(".navbar a, footer a[href='#myPage']").on('click', function(event){

          // Make sure this.hash has a value before overriding default behavior
          if (this.hash !== "") {

               // Prevent default anchor click behavior
               event.preventDefault();

               // Store hash
               var hash = this.hash;

               // Using jQuery's animate() method to add smooth page scroll
               // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
               $('html, body').animate({
               scrollTop: $(hash).offset().top
               }, 900, function(){

               // Add hash (#) to URL when done scrolling (default click behavior)
               window.location.hash = hash;
               });
          } // End if
     });

     //Close Modal Jika Link Daftar di Klik
     $('.daftar').on('click', function(event){
          $('#loginModal').modal('hide');
          $('#forgetpass').modal('hide');
     });

     //Show Password
     var checkbox = $('#tampil-pass');
     var password = $('#password');
     var icon = $('#icon-change');
     checkbox.on('click',function(event) {
          if(checkbox[0].checked == true){
               password[0].type = 'text';
               icon[0].className = 'glyphicon glyphicon-eye-open';
          }else{
               password[0].type = 'password';
               icon[0].className = 'glyphicon glyphicon-eye-close';
          }
     });

     function generate_token(){
       $.ajaxSetup({
         headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
       });
     }

     // $("#form_login").submit(function(event) {
     //      event.preventDefault();
     //      ajax_operation("POST","/WO/tampil-modal","#loginModal");
     // });

     $("#button_reg").click(function(event) {
          $("#pesan_nama").addClass("hidden");
          $("#pesan_notelp").addClass("hidden");
          $("#pesan_email").addClass("hidden");
          $("#pesan_alamat").addClass("hidden");
          $("#pesan_username").addClass("hidden");
          $("#pesan_password").addClass("hidden");
          $("#pesan_jk").addClass("hidden");
          generate_token();
          var formData = $("#form_registrasi").serialize();
          var url = "reg";
          $.ajax({
               type: "POST",
               url: url,
               data: formData,
               beforeSend  : function(){
                   $("#loading").show();
                 },
               success: function(data){
                 $("#loading").hide();
                 swal({
                      title: "SUCCESS",
                      text: "Registrasi Berhasil, Silakan Cek Email Anda",
                      type: "success",
                      confirmButtonColor: "#2b5dcd",
                      confirmButtonText: "OK",
                      closeOnConfirm: true
                 });
                 $("#form_registrasi").trigger('reset'); //Reset form
               },
               error: function(data){
                    $("#loading").hide();
                    var obj = jQuery.parseJSON(data.responseText);
                    $.each(obj,function(key, value) {
                         $("#pesan_"+key).removeClass("hidden");
                         $("#pesan_"+key+" span").html(value);
                    });
               }
          });
     });

     $("#button_log").click(function(event) {
          generate_token();
          var formData = $("#form_login").serialize();
          var url = "log";
          $.ajax({
               type: "POST",
               url: url,
               data: formData,
               beforeSend  : function(){
                   $("#loading").show();
                 },
               success: function(data){
                    console.log(data);
                    $("#loading").hide();
                    switch (data) {
                         case "1":
                              window.location.href = "admin";
                         break;
                         case "2":
                              window.location.href = "member";
                         break;
                         case "3":
                              $("#pesan_error").removeClass('hidden');
                              $("#pesan_error span").html("Akun Anda Belum di Aktivasi");
                         break;
                         case "4":
                              swal({
                              title: "ERROR",
                              text: "Username atau Password Salah",
                              type: "error",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                              });
                         break;
                    }
               }
          });
     });

     $("#button_forgetpass").click(function(event) {
          generate_token();
          var formData = $("#form_forgetpass").serialize();
          var url = "forgetPass";
          $.ajax({
               type: "POST",
               url: url,
               data: formData,
               beforeSend  : function(){
                   $("#loading").show();
               },
               success: function(data){
                 $("#loading").hide();
                 if(data == 1){
                      swal({
                      title: "SUCCESS",
                      text: "Reset Password Berhasil, Silakan Cek Email Anda",
                      type: "success",
                      confirmButtonColor: "#2b5dcd",
                      confirmButtonText: "OK",
                      closeOnConfirm: true
                      });
                 }else{
                      $("#pesan_forpass").removeClass('hidden');
                      $("#pesan_forpass span").html("Alamat Email Yang Anda Masukan Salah");
                 }
               }
          });
     });

}) //End document
