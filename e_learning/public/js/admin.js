//Disable Enter Key
$("input").bind("keypress", function(e) {
      if (e.keyCode == 13) {
          return false;
      }
});

function logout()
{
     $.ajax({
          type: 'GET',
          url: 'admin/logout',
          beforeSend  : function()
          {
              $("#loading").show();
          },
          success: function()
          {
              location.reload();
          }
      });
}

function show_menu(menu){
     ajax_operation("GET","admin/show_menu_"+menu,"#refresh_content");
}


function show_menu_spesifik(menu,id){
     ajax_operation("GET","admin/show-add-"+menu+"/"+id,"#refresh_content");
}

function show_menu_spesifik_data(menu,id){
     ajax_operation("GET","admin/show-add-"+menu+"/"+id,"#refresh_content");
}

function tambah_peserta(menu){
     generate_token();
     ajax_operation("GET","admin/add-peserta/"+menu,"#refresh_content");
     //show_menu_spesifik('subsesi',menu);
}

function kurang_peserta(id,menu){
     generate_token();
     ajax_operation("GET","admin/minus-peserta/"+id,"#refresh_content");
     show_menu_spesifik('subsesi',menu);
}

function cetak_admin(id){
     generate_token();
     ajax_operation("GET","pdfview2/"+id,"#refresh_content");

     show_menu_spesifik_data('showadminmakeclass',id);
}

function cetak_student(id){
     generate_token();
     ajax_operation("GET","pdfview3/"+id,"#refresh_content");
     show_menu_spesifik_data('subsesi',id);
}

function cetak_kelas(id){
     generate_token();
     ajax_operation("GET","pdfview4/"+id,"#refresh_content");
     show_menu_spesifik_data('showclassmakeadmin',id);
}

function hapus_data(nama_menu,id,alamat){
     swal({
          title: "Hapus data "+nama_menu+" dengan nama "+id+"?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#952828",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
     },function(isConfirm){
          if(isConfirm){ //Jika Ya
               generate_token();
               ajax_operation("GET","admin/delete-"+nama_menu+"/"+id,"#refresh_content");
               $("#loading").show();
               show_menu(alamat);
          }else{
               $("#loading").hide();
          }
     });
}

//hapus data jadwal
function hapus_data_jadwal(nama_menu,id,alamat){
     swal({
          title: "Hapus data jadwal "+nama_menu+" pada tanggal "+alamat+"?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#952828",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
     },function(isConfirm){
          if(isConfirm){ //Jika Ya
               generate_token();
               ajax_operation("GET","admin/delete-sesi/"+id,"#refresh_content");
               $("#loading").show();
               show_menu('sesi');
          }else{
               $("#loading").hide();
          }
     });
}

function hapus_data_personel(nama_menu,id,alamat){
     swal({
          title: "Hapus data "+alamat+" dengan nama "+nama_menu+"?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#952828",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
     },function(isConfirm){
          if(isConfirm){ //Jika Ya
               generate_token();
               ajax_operation("GET","admin/delete-"+alamat+"/"+id,"#refresh_content");
               $("#loading").show();
               show_menu(alamat);
          }else{
               $("#loading").hide();
          }
     });
}

function hapus_data_catalog(nama_menu,id,alamat){

     swal({
          title: "Hapus data "+alamat+" dengan nama "+nama_menu+"?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#952828",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
     },function(isConfirm){
          if(isConfirm){ //Jika Ya
               generate_token();
               ajax_operation("GET","admin/delete-"+alamat+"/"+id,"#refresh_content");
               $("#loading").show();
               show_menu(alamat);
          }else{
               $("#loading").hide();
          }
     });
}

//hapus data sub catalog
function hapus_data_subcatalog(nama_menu,id,alamat){

     swal({
          title: "Hapus data "+nama_menu+"?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#952828",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
     },function(isConfirm){
          if(isConfirm){ //Jika Ya
               generate_token();
               ajax_operation("GET","admin/delete-subcatalog/"+id,"#refresh_content");
               $("#loading").show();
               show_menu_spesifik('subcatalog',alamat);
          }else{
               $("#loading").hide();
          }
     });
}


function show_edit(id_users,nama_menu){
     ajax_operation("GET","admin/show-edit-"+nama_menu+"/"+id_users,"#refresh_content");
}

function tampil_edit_jadwal(id_users,nama_menu){
     ajax_operation("GET","admin/show-edit-"+nama_menu+"/"+id_users,"#refresh_content");
}

function show_edit_subcatalog(id_users,nama_menu){
     ajax_operation("GET","admin/show-edit-"+nama_menu+"/"+id_users,"#refresh_content");
}

function show_tambah_subpelatihan(id_users,nama_menu){
     ajax_operation("GET","admin/show-add-"+nama_menu+"/"+id_users,"#refresh_content");
}

function show_tambah_subsesi(id_users,nama_menu){
     ajax_operation("GET","admin/show-add-"+nama_menu+"/"+id_users,"#refresh_content");
}

function show_edit_personel(id_users,nama_menu){
     ajax_operation("GET","admin/show-cari-"+nama_menu+"/"+id_users,"#refresh_content");
}

function show_add_admin(){
     ajax_operation("GET","admin/show-add-admin","#refresh_content");
}

function detail_data(menu,data){
     ajax_operation("GET","admin/detail-"+menu+"/"+data,"#refresh_content");
}




function add_admin(){
     $("#pesan_nama").addClass("hidden");
     $("#pesan_notelp").addClass("hidden");
     $("#pesan_email").addClass("hidden");
     $("#pesan_alamat").addClass("hidden");
     $("#pesan_username").addClass("hidden");
     $("#pesan_password").addClass("hidden");
     $("#pesan_jk").addClass("hidden");
     generate_token();
     var formData = $("#form_add_admin").serialize();
     var url = "admin/add-admin";
     $.ajax({
          type: "POST",
          url: url,
          data: formData,
          beforeSend  : function(){
              $("#loading").show();
            },
          success: function(data){
            $("#loading").hide();
            swal({
                 title: "SUCCESS",
                 text: "Data Admin Berhasil Ditambah",
                 type: "success",
                 confirmButtonColor: "#2b5dcd",
                 confirmButtonText: "OK",
                 closeOnConfirm: true
            },function(){
               //   $("#form_add_admin").trigger('reset');
                $("#loading").show();
                 show_menu('admin');
            });
          },
          error: function(data){
               $("#loading").hide();
               var obj = jQuery.parseJSON(data.responseText);
               $.each(obj,function(key, value) {
                    $("#pesan_"+key).removeClass("hidden");
                    $("#pesan_"+key+" span").html(value);
               });
          }
     });
}

function impottfile(){
     generate_token();
     var formData = $("#form_import").serialize();
     var url = "admin/postImport";
     $.ajax({
          type: "POST",
          url: "admin/show_menu_unit",
          data: formData,
          beforeSend  : function(){
              $("#loading").show();
            },
          success: function(data){
            $("#loading").hide();
            swal({
                 title: "SUCCESS",
                 text: "Data Admin Berhasil Ditambah",
                 type: "success",
                 confirmButtonColor: "#2b5dcd",
                 confirmButtonText: "OK",
                 closeOnConfirm: true
            },function(){
               //   $("#form_add_admin").trigger('reset');
                $("#loading").show();
                 show_menu('personel');
            });
          },
          error: function(data){
               $("#loading").hide();
               var obj = jQuery.parseJSON(data.responseText);
               $.each(obj,function(key, value) {
                    $("#pesan_"+key).removeClass("hidden");
                    $("#pesan_"+key+" span").html(value);
               });
          }
     });
}

function edit_unit()
{
     generate_token();
     $("#pesan_nama_unit").addClass("hidden");
     var formData = $("#form_edit_unit").serialize();
     var url = "admin/edit-unit";
     $.ajax({
          type: "POST",
          url: url,
          data: formData,
          beforeSend  : function()
          {
              $("#loading").show();
          },
          success: function(data)
          {
               $("#loading").hide();
               switch (data) {
                    case "1":
                         swal({
                              title: "ERROR",
                              text: "Unit Sudah Digunakan",
                              type: "error",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         });
                    break;
                    case "2":
                         swal({
                              title: "SUCCESS",
                              text: "Data Admin Berhasil Diupdate",
                              type: "success",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         },function(){
                              $("#loading").show();
                              show_menu('unit');
                         });
                    break;
               }
          },
          error: function(data)
          {
               $("#loading").hide();
               var obj = jQuery.parseJSON(data.responseText);
               $.each(obj,function(key, value){
                    $("#pesan_"+key).removeClass("hidden");
                    $("#pesan_"+key+" span").html(value);
               });
          }
     });
}


//edit personel
function edit_personel()
{
     generate_token();
     $("#pesan_nama_npm").addClass("hidden");
     $("#pesan_nama_personel").addClass("hidden");
     $("#pesan_job").addClass("hidden");
     $("#pesan_unit").addClass("hidden");
     $("#pesan_strata").addClass("hidden");
     var formData = $("#form_edit_personel").serialize();
     var url = "admin/edit-personel";
     $.ajax({
          type: "POST",
          url: url,
          data: formData,
          beforeSend  : function()
          {
              $("#loading").show();
          },
          success: function(data)
          {
               $("#loading").hide();
               switch (data) {
                    case "1":
                         swal({
                              title: "ERROR",
                              text: "No Telp, Email atau Username Sudah Digunakan",
                              type: "error",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         });
                    break;
                    case "2":
                         swal({
                              title: "SUCCESS",
                              text: "Data Admin Berhasil Diupdate",
                              type: "success",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         },function(){
                              $("#loading").show();
                              show_menu('personel');
                         });
                    break;
               }
          },
          error: function(data)
          {
               $("#loading").hide();
               var obj = jQuery.parseJSON(data.responseText);
               $.each(obj,function(key, value){
                    $("#pesan_"+key).removeClass("hidden");
                    $("#pesan_"+key+" span").html(value);
               });
          }
     });
}

//edit personel
function edit_kelas()
{
     generate_token();
     $("#pesan_kelas").addClass("hidden");
     $("#pesan_tgl_mulai").addClass("hidden");
     $("#pesan_tgl_akhir").addClass("hidden");
     $("#pesan_jam_mulai").addClass("hidden");
     $("#pesan_jam_akhir").addClass("hidden");
     $("#pesan_kapasitas").addClass("hidden");
     $("#pesan_pelatih").addClass("hidden");
     $("#pesan_kategori").addClass("hidden");
     $("#pesan_ruangan").addClass("hidden");
     $("#pesan_status").addClass("hidden");
     var formData = $("#form_edit_sesi").serialize();
     var url = "admin/edit-kelas";
     $.ajax({
          type: "POST",
          url: url,
          data: formData,
          beforeSend  : function()
          {
              $("#loading").show();
          },
          success: function(data)
          {
               $("#loading").hide();
               switch (data) {
                    case "1":
                         swal({
                              title: "ERROR",
                              text: "Jadwal Bentrok. Silahkan cek kembali !",
                              type: "error",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         });
                    break;
                    case "2":
                         swal({
                              title: "SUCCESS",
                              text: "Data Jadwal Berhasil Diupdate",
                              type: "success",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         },function(){
                              $("#loading").show();
                              show_menu('sesi');
                         });
                    break;
                    case "3":
                         swal({
                              title: "ERROR",
                              text: "Input Tanggal Salah. Silahkan Coba lagi",
                              type: "error",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         });
                    break;
                    case "4":
                         swal({
                              title: "ERROR",
                              text: "Input Jam Salah. Silahkan Coba lagi",
                              type: "error",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         });
                    break;
               }
          },
          error: function(data)
          {
               $("#loading").hide();
               var obj = jQuery.parseJSON(data.responseText);
               $.each(obj,function(key, value){
                    $("#pesan_"+key).removeClass("hidden");
                    $("#pesan_"+key+" span").html(value);
               });
          }
     });
}


//edit job Roles
function edit_job_roles()
{
     generate_token();
     $("#pesan_nama_job_roles").addClass("hidden");
     var formData = $("#form_edit_job_roles").serialize();
     var url = "admin/edit-job_roles";
     $.ajax({
          type: "POST",
          url: url,
          data: formData,
          beforeSend  : function()
          {
              $("#loading").show();
          },
          success: function(data)
          {
               $("#loading").hide();
               switch (data) {
                    case "1":
                         swal({
                              title: "ERROR",
                              text: "No Telp, Email atau Username Sudah Digunakan",
                              type: "error",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         });
                    break;
                    case "2":
                         swal({
                              title: "SUCCESS",
                              text: "Data Admin Berhasil Diupdate",
                              type: "success",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         },function(){
                              $("#loading").show();
                              show_menu('job');
                         });
                    break;
               }
          },
          error: function(data)
          {
               $("#loading").hide();
               var obj = jQuery.parseJSON(data.responseText);
               $.each(obj,function(key, value){
                    $("#pesan_"+key).removeClass("hidden");
                    $("#pesan_"+key+" span").html(value);
               });
          }
     });
}

//edit location
function edit_location()
{
     generate_token();
     $("#pesan_nama_location").addClass("hidden");
     var formData = $("#form_edit_location").serialize();
     var url = "admin/edit-location";
     $.ajax({
          type: "POST",
          url: url,
          data: formData,
          beforeSend  : function()
          {
              $("#loading").show();
          },
          success: function(data)
          {
               $("#loading").hide();
               switch (data) {
                    case "1":
                         swal({
                              title: "ERROR",
                              text: "No Telp, Email atau Username Sudah Digunakan",
                              type: "error",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         });
                    break;
                    case "2":
                         swal({
                              title: "SUCCESS",
                              text: "Data Admin Berhasil Diupdate",
                              type: "success",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         },function(){
                              $("#loading").show();
                              show_menu('location');
                         });
                    break;
               }
          },
          error: function(data)
          {
               $("#loading").hide();
               var obj = jQuery.parseJSON(data.responseText);
               $.each(obj,function(key, value){
                    $("#pesan_"+key).removeClass("hidden");
                    $("#pesan_"+key+" span").html(value);
               });
          }
     });
}

//edit Catalog
function edit_catalog()
{
     generate_token();
     $("#pesan_nama_catalog").addClass("hidden");
     var formData = $("#form_edit_catalog").serialize();
     var url = "admin/edit-catalog";
     $.ajax({
          type: "POST",
          url: url,
          data: formData,
          beforeSend  : function()
          {
              $("#loading").show();
          },
          success: function(data)
          {
               $("#loading").hide();
               switch (data) {
                    case "1":
                         swal({
                              title: "ERROR",
                              text: "No Telp, Email atau Username Sudah Digunakan",
                              type: "error",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         });
                    break;
                    case "2":
                         swal({
                              title: "SUCCESS",
                              text: "Data Catalog Berhasil Diupdate",
                              type: "success",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         },function(){
                              $("#loading").show();
                              show_menu('catalog');
                         });
                    break;
               }
          },
          error: function(data)
          {
               $("#loading").hide();
               var obj = jQuery.parseJSON(data.responseText);
               $.each(obj,function(key, value){
                    $("#pesan_"+key).removeClass("hidden");
                    $("#pesan_"+key+" span").html(value);
               });
          }
     });
}

function edit_subcatalog(id,alamat)
{
     generate_token();
     $("#pesan_nama_subcatalog").addClass("hidden");
     var formData = $("#form_edit_subcatalog").serialize();
     var url = "admin/edit-subcatalog";
     $.ajax({
          type: "POST",
          url: url,
          data: formData,
          beforeSend  : function()
          {
              $("#loading").show();
          },
          success: function(data)
          {
               $("#loading").hide();
               switch (data) {
                    case "1":
                         swal({
                              title: "ERROR",
                              text: "No Telp, Email atau Username Sudah Digunakan",
                              type: "error",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         });
                    break;
                    case "2":
                         swal({
                              title: "SUCCESS",
                              text: "Data Berhasil Diupdate",
                              type: "success",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         },function(){
                              $("#loading").show();
                              show_tambah_subpelatihan(id,alamat);
                         });
                    break;
               }
          },
          error: function(data)
          {
               $("#loading").hide();
               var obj = jQuery.parseJSON(data.responseText);
               $.each(obj,function(key, value){
                    $("#pesan_"+key).removeClass("hidden");
                    $("#pesan_"+key+" span").html(value);
               });
          }
     });
}


function add_data_kelas(){
     $("#pesan_kelas").addClass("hidden");
     $("#pesan_tgl_mulai").addClass("hidden");
     $("#pesan_tgl_akhir").addClass("hidden");
     $("#pesan_jam_mulai").addClass("hidden");
     $("#pesan_jam_akhir").addClass("hidden");
     $("#pesan_tot_waktu").addClass("hidden");
     $("#pesan_kapasitas").addClass("hidden");
     $("#pesan_pelatih").addClass("hidden");
     $("#pesan_kategori").addClass("hidden");
     $("#pesan_ruangan").addClass("hidden");
     generate_token();
     var formData = $("#form_add_menu").serialize();
     var url = "admin/add-jadwal";
     $.ajax({
          type: "POST",
          url: url,
          data: formData,
          beforeSend  : function(){
              $("#loading").show();
            },
          success: function(data){
            $("#loading").hide();
            if(data == "1"){
                 swal({
                      title: "ERROR",
                      text: "Jadwal Kelas Terjadi Bentrok. Silahkan Cek Kembali !",
                      type: "error",
                      confirmButtonColor: "#2b5dcd",
                      confirmButtonText: "OK",
                      closeOnConfirm: true
                 });
            }else if(data == "2"){
                 swal({
                      title: "ERROR",
                      text: "Input Tanggal Salah. Silahkan Cek Kembali !",
                      type: "error",
                      confirmButtonColor: "#2b5dcd",
                      confirmButtonText: "OK",
                      closeOnConfirm: true
                 });
            }else if(data == "3"){
                 swal({
                      title: "ERROR",
                      text: "Input Jam Salah. Silahkan Cek Kembali !",
                      type: "error",
                      confirmButtonColor: "#2b5dcd",
                      confirmButtonText: "OK",
                      closeOnConfirm: true
                 });
            }else{
                 swal({
                      title: "SUCCESS",
                      text: "Data Jadwal Berhasil Ditambah",
                      type: "success",
                      confirmButtonColor: "#2b5dcd",
                      confirmButtonText: "OK",
                      closeOnConfirm: true
                 },function(){
                      show_menu('sesi');
                 });
            }
          },
          error: function(data){
               $("#loading").hide();
               var obj = jQuery.parseJSON(data.responseText);
               $.each(obj,function(key, value) {
                    $("#pesan_"+key).removeClass("hidden");
                    $("#pesan_"+key+" span").html(value);
               });
          }
     });
}

function add_data_matpel(){

     $("#pesan_nama_matpel").addClass("hidden");
     generate_token();
     var formData = $("#form_add_matpel").serialize();
     var url = "admin/add_matpel";
     $.ajax({
          type: "POST",
          url: url,
          data: formData,
          beforeSend  : function(){
              $("#loading").show();
         },
          success: function(data){
            $("#loading").hide();
            if(data == "1"){
                 swal({
                    title: "ERROR",
                    text: "Nama Mata Pelajaran Sudah Digunakan",
                    type: "error",
                    confirmButtonColor: "#2b5dcd",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
               });
            }else{
                 swal({
                      title: "SUCCESS",
                      text: "Data Mata Pelajaran Berhasil Ditambah",
                      type: "success",
                      confirmButtonColor: "#2b5dcd",
                      confirmButtonText: "OK",
                      closeOnConfirm: true
                 },function(){
                      //show_menu('matpel');
                 });
            }
          },
          error: function(data){
               $("#loading").hide();
               var obj = jQuery.parseJSON(data.responseText);
               $.each(obj,function(key, value) {
                    console.log(key);
                    $("#pesan_"+key).removeClass("hidden");
                    $("#pesan_"+key+" span").html(value);
               });
          }
     });
}
//tambah data job roles
function add_data_job_roles(){
     $("#pesan_nama_job_roles").addClass("hidden");
     generate_token();
     var formData = $("#form_add_job_roles").serialize();
     var url = "admin/add-job_roles";
     $.ajax({
          type: "POST",
          url: url,
          data: formData,
          beforeSend  : function(){
              $("#loading").show();
         },
          success: function(data){
            $("#loading").hide();
            if(data == "1"){
                 swal({
                    title: "ERROR",
                    text: "Nama Job Roles Sudah Digunakan",
                    type: "error",
                    confirmButtonColor: "#2b5dcd",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
               });
            }else{
                 swal({
                      title: "SUCCESS",
                      text: "Data Job Roles Berhasil Ditambah",
                      type: "success",
                      confirmButtonColor: "#2b5dcd",
                      confirmButtonText: "OK",
                      closeOnConfirm: true
                 },function(){
                      show_menu('job');
                 });
            }
          },
          error: function(data){
               $("#loading").hide();
               var obj = jQuery.parseJSON(data.responseText);
               $.each(obj,function(key, value) {
                    console.log(key);
                    $("#pesan_"+key).removeClass("hidden");
                    $("#pesan_"+key+" span").html(value);
               });
          }
     });
}


//tambah data location
function add_data_location(){
     $("#pesan_nama_location").addClass("hidden");
     generate_token();
     var formData = $("#form_add_location").serialize();
     var url = "admin/add-location";
     $.ajax({
          type: "POST",
          url: url,
          data: formData,
          beforeSend  : function(){
              $("#loading").show();
         },
          success: function(data){
            $("#loading").hide();
            if(data == "1"){
                 swal({
                    title: "ERROR",
                    text: "Nama Location Sudah Digunakan",
                    type: "error",
                    confirmButtonColor: "#2b5dcd",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
               });
            }else{
                 swal({
                      title: "SUCCESS",
                      text: "Data Location Berhasil Ditambah",
                      type: "success",
                      confirmButtonColor: "#2b5dcd",
                      confirmButtonText: "OK",
                      closeOnConfirm: true
                 },function(){
                      show_menu('location');
                 });
            }
          },
          error: function(data){
               $("#loading").hide();
               var obj = jQuery.parseJSON(data.responseText);
               $.each(obj,function(key, value) {
                    console.log(key);
                    $("#pesan_"+key).removeClass("hidden");
                    $("#pesan_"+key+" span").html(value);
               });
          }
     });
}

//tambah data catalog
function add_data_catalog(){
     $("#pesan_nama_catalog").addClass("hidden");
     generate_token();
     var formData = $("#form_add_catalog").serialize();
     var url = "admin/add-catalog";
     $.ajax({
          type: "POST",
          url: url,
          data: formData,
          beforeSend  : function(){
              $("#loading").show();
         },
          success: function(data){
            $("#loading").hide();
            if(data == "1"){
                 swal({
                    title: "ERROR",
                    text: "Nama Catalog Sudah Digunakan",
                    type: "error",
                    confirmButtonColor: "#2b5dcd",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
               });
            }else{
                 swal({
                      title: "SUCCESS",
                      text: "Data Catalog Berhasil Ditambah",
                      type: "success",
                      confirmButtonColor: "#2b5dcd",
                      confirmButtonText: "OK",
                      closeOnConfirm: true
                 },function(){
                      show_menu('catalog');
                 });
            }
          },
          error: function(data){
               $("#loading").hide();
               var obj = jQuery.parseJSON(data.responseText);
               $.each(obj,function(key, value) {
                    console.log(key);
                    $("#pesan_"+key).removeClass("hidden");
                    $("#pesan_"+key+" span").html(value);
               });
          }
     });
}

//tambah data catalog
function add_data_subcatalog(id){
     $("#pesan_nama_kelas").addClass("hidden");
     generate_token();
     var formData = $("#form_add_subcatalog").serialize();
     var url = "admin/add-subcatalog";
     $.ajax({
          type: "POST",
          url: url,
          data: formData,
          beforeSend  : function(){
              $("#loading").show();
         },
          success: function(data){
            $("#loading").hide();
            if(data == "1"){
                 swal({
                    title: "ERROR",
                    text: "Nama Catalog Sudah Digunakan",
                    type: "error",
                    confirmButtonColor: "#2b5dcd",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
               });
            }else{
                 swal({
                      title: "SUCCESS",
                      text: "Data Catalog Berhasil Ditambah",
                      type: "success",
                      confirmButtonColor: "#2b5dcd",
                      confirmButtonText: "OK",
                      closeOnConfirm: true
                 },function(){
                      show_menu_spesifik('subcatalog',id);
                 });
            }
          },
          error: function(data){
               $("#loading").hide();
               var obj = jQuery.parseJSON(data.responseText);
               $.each(obj,function(key, value) {
                    console.log(key);
                    $("#pesan_"+key).removeClass("hidden");
                    $("#pesan_"+key+" span").html(value);
               });
          }
     });
}


//tambah data categori
function add_data_categori(){
     $("#pesan_nama_categori").addClass("hidden");
     generate_token();
     var formData = $("#form_add_categori").serialize();
     var url = "admin/add-categori";
     $.ajax({
          type: "POST",
          url: url,
          data: formData,
          beforeSend  : function(){
              $("#loading").show();
         },
          success: function(data){
            $("#loading").hide();
            if(data == "1"){
                 swal({
                    title: "ERROR",
                    text: "Nama Kategori Sudah Digunakan",
                    type: "error",
                    confirmButtonColor: "#2b5dcd",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
               });
            }else{
                 swal({
                      title: "SUCCESS",
                      text: "Data Kategori Berhasil Ditambah",
                      type: "success",
                      confirmButtonColor: "#2b5dcd",
                      confirmButtonText: "OK",
                      closeOnConfirm: true
                 },function(){
                      show_menu('catalog');
                 });
            }
          },
          error: function(data){
               $("#loading").hide();
               var obj = jQuery.parseJSON(data.responseText);
               $.each(obj,function(key, value) {
                    console.log(key);
                    $("#pesan_"+key).removeClass("hidden");
                    $("#pesan_"+key+" span").html(value);
               });
          }
     });
}




//tambah data bidang
function add_data_bidang(){
     $("#pesan_nama_bidang").addClass("hidden");
     generate_token();
     var formData = $("#form_add_bidang").serialize();
     var url = "admin/add-bidang";
     $.ajax({
          type: "POST",
          url: url,
          data: formData,
          beforeSend  : function(){
              $("#loading").show();
         },
          success: function(data){
            $("#loading").hide();
            if(data == "1"){
                 swal({
                    title: "ERROR",
                    text: "Nama Bidang Sudah Digunakan",
                    type: "error",
                    confirmButtonColor: "#2b5dcd",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
               });
            }else{
                 swal({
                      title: "SUCCESS",
                      text: "Data Bidang Berhasil Ditambah",
                      type: "success",
                      confirmButtonColor: "#2b5dcd",
                      confirmButtonText: "OK",
                      closeOnConfirm: true
                 },function(){
                      show_menu('personel');
                 });
            }
          },
          error: function(data){
               $("#loading").hide();
               var obj = jQuery.parseJSON(data.responseText);
               $.each(obj,function(key, value) {
                    console.log(key);
                    $("#pesan_"+key).removeClass("hidden");
                    $("#pesan_"+key+" span").html(value);
               });
          }
     });
}


//tambah data personel
function add_data_personel(){
     $("#pesan_nama_personel").addClass("hidden");
     $("#pesan_unit").addClass("hidden");
     $("#pesan_job").addClass("hidden");
     $("#pesan_strata").addClass("hidden");
     $("#pesan_nama_npm").addClass("hidden");
     generate_token();
     var formData = $("#form_add_personel").serialize();
     var url = "admin/add-personel";
     $.ajax({
          type: "POST",
          url: url,
          data: formData,
          beforeSend  : function(){
              $("#loading").show();
         },
          success: function(data){
            $("#loading").hide();
            if(data == "1"){
                 swal({
                    title: "ERROR",
                    text: "Username Sudah Digunakan",
                    type: "error",
                    confirmButtonColor: "#2b5dcd",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
               });
            }else{
                 swal({
                      title: "SUCCESS",
                      text: "Data Personel Berhasil Ditambah",
                      type: "success",
                      confirmButtonColor: "#2b5dcd",
                      confirmButtonText: "OK",
                      closeOnConfirm: true
                 },function(){
                      show_menu('personel');
                 });
            }
          },
          error: function(data){
               $("#loading").hide();
               var obj = jQuery.parseJSON(data.responseText);
               $.each(obj,function(key, value) {
                    console.log(key);
                    $("#pesan_"+key).removeClass("hidden");
                    $("#pesan_"+key+" span").html(value);
               });
          }
     });
}


function getData(hal, content,menu){
     $.ajax({
          type: "GET",
          url: 'admin/get-data-'+menu+'?page='+hal,
          beforeSend  : function(){
               $("#loading").show();
          },
          success: function(data){
               $("#loading").hide();
               if(content!=""){
                    $(content).html(data);
               }
          }
     });
}

function cari_data(menu,form){
     var d = $("#search").val();
     var url = "admin/cari-data-"+menu;
     var data = $(form).serializeArray();
     $.ajax({
          type: "GET",
          url: url,
          data: data,
          beforeSend  : function(){
               $("#loading").show();
          },
          success: function(data){
               $("#loading").hide();
               if(data == "1"){
                    swal({
                         title: "ERROR",
                         text: "Data "+menu+" yang dimasukan tidak boleh kosong",
                         type: "error",
                         confirmButtonColor: "#2b5dcd",
                         confirmButtonText: "OK",
                         closeOnConfirm: true
                    },function(){
                         show_menu(menu);
                    });

               }else if(data == "2"){
                    swal({
                         title: "ERROR",
                         text: "Data "+menu+" yang dicari tidak ada",
                         type: "error",
                         confirmButtonColor: "#2b5dcd",
                         confirmButtonText: "OK",
                         closeOnConfirm: true
                    },function(){
                         show_menu(menu);
                    });
               }else{
                    $("#refresh_content").html(data);
               }
          }
     })
}

function get_data_detail(hal,search,content,menu){
     $.ajax({
         type: "GET",
         url: 'admin/cari-data-detail-'+menu+'?page='+hal,
         data : {'search' : search},
         beforeSend  : function(){
              $("#loading").show();
         },
         success: function(data){
              $("#loading").hide();
              if(content!=""){
                   $(content).html(data);
              }
         }
    });
}

function unggah()
{
     generate_token();
     var formData = $("#form_import").serialize();
     var url = "admin/postImport";
     $.ajax({
          type: "POST",
          url: url,
          data: formData,
          beforeSend  : function()
          {
              $("#loading").show();
          },
          success: function(data)
          {
               $("#loading").hide();
               switch (data) {
                    case "1":
                         swal({
                              title: "Gagal Import",
                              text: "File diizinkan .xls, .xlxs",
                              type: "error",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         });
                    break;
                    case "2":
                         swal({
                              title: "SUCCESS",
                              text: "Data Berhasil di Import",
                              type: "success",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         },function(){
                              $("#loading").show();
                              show_menu('personel');
                         });
                    break;
               }
          },
          error: function(data)
          {
               $("#loading").hide();
               var obj = jQuery.parseJSON(data.responseText);
               $.each(obj,function(key, value){
                    $("#pesan_"+key).removeClass("hidden");
                    $("#pesan_"+key+" span").html(value);
               });
          }
     });
}

//ubah status sub sesi
function ubahstatus_subsesi(nama_menu,id,alamat){
     swal({
          title: "Ubah Status "+nama_menu+" pada tanggal "+alamat+"?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#952828",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
     },function(isConfirm){
          if(isConfirm){ //Jika Ya
               generate_token();
               ajax_operation("GET","admin/edit-status-subsesi/"+id,"#refresh_content");
               $("#loading").show();
               show_menu('sesi');
          }else{
               $("#loading").hide();
          }
     });
}
