function generate_token(){
     $.ajaxSetup({
          headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
     });
}

function browse_file()
{
     $("#upload").click();
}

function tampil_gambar(input)
{
     var _validFileExtensions = [".jpg", ".jpeg"];
     var file_size = input.files[0].size;
     if (input.type == "file")
     {
          var sFileName = input.value; //ambil nilai dari input file
          if (sFileName.length > 0)
          {
               var blnValid = false;
               for (var j = 0; j < _validFileExtensions.length; j++)
               {
                    var sCurExtension = _validFileExtensions[j];
                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase())
                    {
                         blnValid = true;
                         break;
                    }
               }
               if (!blnValid)
               {
                    swal({
                         title: "Error",
                         text: "Maaf, ekstensi file ini tidak cocok,\nberikut ekstensi yang diizinkan "+_validFileExtensions.join(", "),
                         type: "error",
                         confirmButtonColor: "#2b5dcd",
                         confirmButtonText: "OK",
                         closeOnConfirm: true
                    });
                    return false;
               }else{
                    if (input.files && input.files[0])
                    {
                         console.log(file_size);
                         if(file_size > 2097152){ //Jika Ukuran File Lebih dari 2mb
                              swal({
                                   title: "Error",
                                   text: "Ukuran file harus di bawah 2MB",
                                   type: "error",
                                   confirmButtonColor: "#2b5dcd",
                                   confirmButtonText: "OK",
                                   closeOnConfirm: true
                              });
                              return false;
                         }
                         var reader = new FileReader();
                         reader.onload = function (e)
                         {
                              $('#gambar_akun')
                              .attr('src', e.target.result)
                              .width('100%')
                              .height('85%');
                         };
                         reader.readAsDataURL(input.files[0]);
                    }
                    var url_gambar = $("#upload").val();
               }
          }
     }

}

$(document).ready(function()
{
     if(window.location.href.indexOf('#loginModal') != -1)
     {
          $('#loginModal').modal('show');
     }else if(window.location.href.indexOf('#forgetpass') != -1)
     {
          $('#forgetpass').modal('show');
     }

     $('button.close').click(function(event){
          history.pushState("", document.title, window.location.pathname);
     });
});

function up_data()
{
     $("#pesan_nama").addClass("hidden");
     $("#pesan_notelp").addClass("hidden");
     $("#pesan_email").addClass("hidden");
     $("#pesan_alamat").addClass("hidden");
     $("#pesan_username").addClass("hidden");
     $("#pesan_password").addClass("hidden");
     $("#pesan_jk").addClass("hidden");
     generate_token();
     // var formData = $("#form_update").serialize();
     var formData = new FormData($("#form_update")[0]);
     var url = "/WO/member/up";
     $.ajax({
          type: "POST",
          url: url,
          data: formData,
          processData: false,
          contentType: false,
          beforeSend  : function()
          {
              $("#loading").show();
          },
          success: function(data)
          {
               $("#loading").hide();
               switch (data) {
                    case "1":
                         swal({
                              title: "ERROR",
                              text: "No Telp, Email atau Username Sudah Digunakan",
                              type: "error",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         });
                    break;
                    case "2":
                         swal({
                              title: "SUCCESS",
                              text: "Data Berhasil Diupdate",
                              type: "success",
                              confirmButtonColor: "#2b5dcd",
                              confirmButtonText: "OK",
                              closeOnConfirm: true
                         });
                    break;
               }
          },
          error: function(data)
          {
               $("#loading").hide();
               var obj = jQuery.parseJSON(data.responseText);
               $.each(obj,function(key, value){
                    $("#pesan_"+key).removeClass("hidden");
                    $("#pesan_"+key+" span").html(value);
               });
          }
     });
}
