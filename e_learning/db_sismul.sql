-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 24, 2017 at 03:18 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sismul`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_jawaban`
--

CREATE TABLE `detail_jawaban` (
  `id_jawaban` int(11) DEFAULT NULL,
  `skor` int(11) DEFAULT NULL,
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_jawaban`
--

INSERT INTO `detail_jawaban` (`id_jawaban`, `skor`, `id`) VALUES
(1, 1, 1),
(1, 1, 4),
(1, 0, 8),
(1, 1, 5),
(2, 0, 7),
(2, 1, 5),
(2, 0, 1),
(2, 1, 4),
(9, 1, 2),
(9, 0, 5),
(9, 0, 4),
(9, 1, 7),
(13, 0, 4),
(13, 0, 5),
(13, 0, 9),
(13, 1, 7),
(14, 0, 9),
(14, 0, 1),
(14, 1, 7),
(14, 0, 8);

-- --------------------------------------------------------

--
-- Table structure for table `jawaban`
--

CREATE TABLE `jawaban` (
  `id_jawaban` int(11) NOT NULL,
  `id_ujian` int(11) DEFAULT NULL,
  `id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jawaban`
--

INSERT INTO `jawaban` (`id_jawaban`, `id_ujian`, `id`) VALUES
(1, 1, 1),
(2, 2, 1),
(9, 3, 1),
(10, 4, 1),
(13, 5, 1),
(14, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mata_pelajaran`
--

CREATE TABLE `mata_pelajaran` (
  `id_matpel` int(11) NOT NULL,
  `nama_matpel` varchar(20) DEFAULT NULL,
  `jurusan` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mata_pelajaran`
--

INSERT INTO `mata_pelajaran` (`id_matpel`, `nama_matpel`, `jurusan`) VALUES
(1, 'Sosiologi', 'IPS'),
(2, 'sejarah', 'IPS');

-- --------------------------------------------------------

--
-- Table structure for table `materi`
--

CREATE TABLE `materi` (
  `id_materi` int(11) NOT NULL,
  `id_matpel` int(11) DEFAULT NULL,
  `nama_materi` text,
  `deskripsi` text,
  `file` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `materi`
--

INSERT INTO `materi` (`id_materi`, `id_matpel`, `nama_materi`, `deskripsi`, `file`) VALUES
(1, 1, 'asdasd', 'asdasd', '1513969228-AViVA - GRRRLS.mp4'),
(2, 1, 'asdasd', 'asdasdas', '1514124326-ini video format mp4.mp4'),
(3, 2, 'Via vallen', 'keren', '1514125276-Via Vallen - Sayang (Official Music Video).mp4');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `random_soal`
--

CREATE TABLE `random_soal` (
  `id_random` int(11) NOT NULL,
  `id_user` int(10) UNSIGNED DEFAULT NULL,
  `id_soal` int(11) DEFAULT NULL,
  `id_ujian` int(11) DEFAULT NULL,
  `jawaban_siswa` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `random_soal`
--

INSERT INTO `random_soal` (`id_random`, `id_user`, `id_soal`, `id_ujian`, `jawaban_siswa`) VALUES
(1, 1, 4, 1, NULL),
(2, 1, 7, 1, NULL),
(3, 1, 2, 1, NULL),
(4, 1, 1, 1, NULL),
(46, 1, 2, 3, 'B'),
(47, 1, 5, 3, 'C'),
(48, 1, 4, 3, 'D'),
(49, 1, 7, 3, 'E'),
(59, 1, 4, 5, 'C'),
(60, 1, 5, 5, 'B'),
(61, 1, 9, 5, 'C'),
(62, 1, 7, 5, 'E'),
(63, 1, 8, 6, 'E'),
(64, 1, 9, 6, 'D'),
(65, 1, 1, 6, 'E'),
(66, 1, 7, 6, 'E');

-- --------------------------------------------------------

--
-- Table structure for table `soal`
--

CREATE TABLE `soal` (
  `id` int(11) NOT NULL,
  `id_matpel` int(11) DEFAULT NULL,
  `pertanyaan` text,
  `jawaban_a` varchar(200) DEFAULT NULL,
  `jawaban_b` varchar(200) DEFAULT NULL,
  `jawaban_c` varchar(200) DEFAULT NULL,
  `jawaban_d` varchar(200) DEFAULT NULL,
  `jawaban_e` varchar(200) DEFAULT NULL,
  `kunci_jawaban` char(1) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal`
--

INSERT INTO `soal` (`id`, `id_matpel`, `pertanyaan`, `jawaban_a`, `jawaban_b`, `jawaban_c`, `jawaban_d`, `jawaban_e`, `kunci_jawaban`, `file`) VALUES
(1, 2, 'Apa penyebab dinosurus punah?', 'Meteor', 'Pemanasan Global', 'Pemburuan Liar', 'Tsunami', 'Kurang Micin', 'A', NULL),
(2, 2, 'Kapan terjadi jaman batu?', '200 tahun lalu', '400 tahun lalu', '600 tahun lalu', '800 tahun lalu', '1000 tahun lalu', 'B', NULL),
(3, 1, 'Apa penyebab seseorang susah bersosialisasi?', 'Kurang Berani', 'Malu-malu kucing', 'Malu tapi mau', 'Cuek', 'Banyak Makan Micin', 'E', NULL),
(4, 2, 'sdd', 'd', 'd', 's', 'dee', 'ss', 'B', NULL),
(5, 2, 'Dimana letak sekoloa ', 'bandung', 'belakang unikom', 'dekat dago', 'dejat kedokteran unpad', 'semua jawaban benar', 'E', NULL),
(6, 2, 'asdfasdfdsaf', 'eeee', 'ww', 'ss', 'fcf', 'vfe', 'C', NULL),
(7, 2, 'Dimana letak bandung ???', 'sdf', 'ddsdfg', 'ddcv hjhsjhfg', 'www', 'erer', 'E', NULL),
(8, 2, 'dimana letak unikom', 'ddd', 'dd', 'we', 'we', 'rrr', 'D', NULL),
(9, 2, 'Berasal dari mana kata sejarah', 'adf', 'ddd', 'sssdfdfa', 'dad', 'sdf', 'B', NULL),
(10, 2, 'saasdasd', 'asdasda', 'asdasd', 'asd', 'asd', 'ad', 'C', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ujian`
--

CREATE TABLE `ujian` (
  `id_ujian` int(11) NOT NULL,
  `id_matpel` int(11) DEFAULT NULL,
  `nama_ujian` varchar(20) DEFAULT NULL,
  `durasi` int(11) DEFAULT NULL,
  `jumlah_soal` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ujian`
--

INSERT INTO `ujian` (`id_ujian`, `id_matpel`, `nama_ujian`, `durasi`, `jumlah_soal`) VALUES
(1, 2, 'Latihan 1 2', 20, 4),
(2, 2, 'Latihan Sejaran 2 45', 10, 4),
(3, 2, 'UTS Sejarah', 10, 4),
(4, 1, 'Latihan Sosilogi', 2, 4),
(5, 2, 'Uas Sejarah 1', 10, 5),
(6, 2, 'Uas Sejarah 2', 30, 4);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_permission` int(11) NOT NULL,
  `jurusan` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_matpel` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `is_permission`, `jurusan`, `nama_matpel`, `created_at`, `updated_at`) VALUES
(1, 'Aku Siswa', 'siswa@sismul.dev', '$2y$10$rosbaxi7iTYex8g98MndFugkZ0QA0vOB8KkoJL0GOggxGjw6DsB0a', 'XjYdImR163mLL5KkPgyNiAHGqb0fwPhuATnUOFAuwSOiOQ9d9M9dGNsnxdyM', 1, 'IPS', NULL, NULL, '2017-12-24 07:02:18'),
(2, 'Aku Guru', 'guru@sismul.dev', '$2y$10$boJLKCRA3Lb3BtInmRPZtOBu.2iE2USBzotwB6EA6K3/zJguXTpkq', 'jcbXEjHvaGiDm1Bykpn7cF37M8KHdcGJnLHjnmsmJoFcnQgPD5u7HL7hTqo4', 2, 'IPS', 'sejarah', NULL, '2017-12-19 10:09:48'),
(3, 'Aku Admin', 'admin@sismul.dev', '$2y$10$2EnMWIrP8BKAp.AHuNFTae0aGT2jsdEWSYFJILpi/XRJG4HD2kxie', 'coojehwuEFBJcToHOPD4YKyCqjd0kmXe7fNb2wkr0ru37CvDFbfQ0JEyzM23', 3, NULL, NULL, NULL, '2017-12-19 10:01:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_jawaban`
--
ALTER TABLE `detail_jawaban`
  ADD KEY `id_jawaban` (`id_jawaban`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `jawaban`
--
ALTER TABLE `jawaban`
  ADD PRIMARY KEY (`id_jawaban`),
  ADD KEY `id_ujian` (`id_ujian`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  ADD PRIMARY KEY (`id_matpel`);

--
-- Indexes for table `materi`
--
ALTER TABLE `materi`
  ADD PRIMARY KEY (`id_materi`),
  ADD KEY `id_matpel` (`id_matpel`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `random_soal`
--
ALTER TABLE `random_soal`
  ADD PRIMARY KEY (`id_random`),
  ADD KEY `id_soal` (`id_soal`),
  ADD KEY `id` (`id_user`),
  ADD KEY `id_ujian` (`id_ujian`);

--
-- Indexes for table `soal`
--
ALTER TABLE `soal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_matpel` (`id_matpel`);

--
-- Indexes for table `ujian`
--
ALTER TABLE `ujian`
  ADD PRIMARY KEY (`id_ujian`),
  ADD KEY `id_matpel` (`id_matpel`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jawaban`
--
ALTER TABLE `jawaban`
  MODIFY `id_jawaban` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  MODIFY `id_matpel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `materi`
--
ALTER TABLE `materi`
  MODIFY `id_materi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `random_soal`
--
ALTER TABLE `random_soal`
  MODIFY `id_random` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `soal`
--
ALTER TABLE `soal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `ujian`
--
ALTER TABLE `ujian`
  MODIFY `id_ujian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_jawaban`
--
ALTER TABLE `detail_jawaban`
  ADD CONSTRAINT `detail_jawaban_ibfk_1` FOREIGN KEY (`id_jawaban`) REFERENCES `jawaban` (`id_jawaban`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_jawaban_ibfk_2` FOREIGN KEY (`id`) REFERENCES `soal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `jawaban`
--
ALTER TABLE `jawaban`
  ADD CONSTRAINT `jawaban_ibfk_1` FOREIGN KEY (`id_ujian`) REFERENCES `ujian` (`id_ujian`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jawaban_ibfk_2` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `materi`
--
ALTER TABLE `materi`
  ADD CONSTRAINT `materi_ibfk_1` FOREIGN KEY (`id_matpel`) REFERENCES `mata_pelajaran` (`id_matpel`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `random_soal`
--
ALTER TABLE `random_soal`
  ADD CONSTRAINT `random_soal_ibfk_1` FOREIGN KEY (`id_soal`) REFERENCES `soal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `random_soal_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `random_soal_ibfk_3` FOREIGN KEY (`id_ujian`) REFERENCES `ujian` (`id_ujian`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `soal`
--
ALTER TABLE `soal`
  ADD CONSTRAINT `soal_ibfk_1` FOREIGN KEY (`id_matpel`) REFERENCES `mata_pelajaran` (`id_matpel`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ujian`
--
ALTER TABLE `ujian`
  ADD CONSTRAINT `ujian_ibfk_1` FOREIGN KEY (`id_matpel`) REFERENCES `mata_pelajaran` (`id_matpel`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
