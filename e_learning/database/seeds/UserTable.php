<?php

use Illuminate\Database\Seeder;

class UserTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
       'name' => 'Aku Siswa',
       'email' => 'siswa@sismul.dev',
       'password' => bcrypt('rahasia'),
      ]);

        DB::table('users')->insert([
       'name' => 'Aku Guru',
       'email' => 'guru@sismul.dev',
       'password' => bcrypt('rahasia'),
      ]);

        DB::table('users')->insert([
       'name' => 'Aku Pegawai',
       'email' => 'pegawai@sismul.dev',
       'password' => bcrypt('rahasia'),
      ]);
    }
}
