<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ujian extends Model
{
    public $timestamps = false;
    public $primaryKey = 'id_ujian';
    protected $table = 'ujian';
}
