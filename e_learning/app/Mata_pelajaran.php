<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mata_pelajaran extends Model
{
    public $timestamps = false;
    protected $table = 'mata_pelajaran';
    protected $fillable =['id_matpel','nama_matpel','jurusan'];
}
