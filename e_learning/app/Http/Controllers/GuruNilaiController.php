<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\MataPelajaran;
use App\GuruUjian;
use App\User;
use DB;
use Auth;

class GuruNilaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matpel = MataPelajaran::select('id_matpel')
                     ->where('nama_matpel', '=', ''.Auth::user()->nama_matpel.'')
                     ->get();
        foreach ($matpel as $key => $value) {
            $id_matpel = $value->id_matpel;
        }
        $ujian = GuruUjian::select('id_ujian', 'id_matpel', 'nama_ujian', 'durasi', 'jumlah_soal')
                 ->where('id_matpel', '=', ''.$id_matpel.'')
                 ->get();
        return view('layouts.guru.nilai.index', ['name'=> Auth::user()->name, 'ujian'=> $ujian, 'nama_matpel'=> Auth::user()->nama_matpel]);
    }

    public function index_siswa($id)
    {
        $siswa = GuruUjian::select('jawaban.id', 'users.name')
                 ->join('jawaban', 'ujian.id_ujian', '=', 'jawaban.id_ujian')
                 ->join('users', 'users.id', '=', 'jawaban.id')
                 ->where('ujian.id_matpel', '=', ''.$id.'')
                 ->distinct()
                 ->get();

        return view('layouts.guru.siswa.index', ['name'=> Auth::user()->name, 'ujian'=> $siswa, 'nama_matpel'=> Auth::user()->nama_matpel]);
    }

    public function index_nilai_siswa($id)
    {

        // $siswa = GuruUjian::select('jawaban.id','users.name')
        //          ->join('jawaban','ujian.id_ujian','=','jawaban.id_ujian')
        //          ->join('users','users.id','=','jawaban.id')
        //          ->where('ujian.id_matpel','=',''.$id.'')
        //          ->distinct()
        //          ->get();

        $nilai = User::select('mata_pelajaran.nama_matpel', 'ujian.nama_ujian', DB::raw('(100/ujian.jumlah_soal)*sum(detail_jawaban.skor) nilai'))
                 ->join('jawaban', 'users.id', '=', 'jawaban.id')
                 ->join('ujian', 'jawaban.id_ujian', '=', 'ujian.id_ujian')
                 ->join('mata_pelajaran', 'mata_pelajaran.id_matpel', '=', 'ujian.id_matpel')
                 ->join('detail_jawaban', 'jawaban.id_jawaban', '=', 'detail_jawaban.id_jawaban')
                 ->where('users.id', '=', ''.$id.'', 'and', 'mata_pelajaran.nama_matpel', '=', ''.Auth::user()->name.'', 'and', 'detail_jawaban.skor', '=', '1')
                 ->groupBY('ujian.nama_ujian')
                 ->get();
        // $nama_matpel = 'sejarah';
        // $nilai = DB::select("select c.nama_ujian, (100/c.jumlah_soal)*count(c.nama_ujian) nilai
        //          from users a
        //               join jawaban b on a.id=b.id
        //               join ujian c on b.id_ujian=c.id_ujian
        //               join mata_pelajaran d on c.id_matpel=d.id_matpel
        //               join detail_jawaban e on b.id_jawaban=e.id_jawaban
        //               where (a.id='.$id.' and d.nama_matpel = '".$nama_matpel."') and e.skor=1
        //               group by c.nama_ujian");



        return view('layouts.guru.hasil_nilai.index', ['name'=> Auth::user()->name, 'ujian'=> $nilai, 'nama_matpel'=> Auth::user()->nama_matpel]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
       'id_matpel' => 'required',
       'nama_ujian' => 'required',
       'durasi' => 'required',
       'jumlah_soal' => 'required',
      ]);

        $ujian = new GuruUjian;
        $ujian->id_matpel = $request->id_matpel;
        $ujian->nama_ujian = $request->nama_ujian;
        $ujian->durasi = $request->durasi;
        $ujian->jumlah_soal = $request->jumlah_soal;
        $ujian->save();
        return back()->with('msg', '<b> <center>Soal Berhasil Ditambahkan<i class="mdi mdi-check"></i></b> </center> ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      //validasi form
        $this->validate($request, [
       'nama_ujian' => 'required',
       'durasi' => 'required',
       'jumlah_soal' => 'required',
      ]);

        //update mata pelajaran
        // $ujian = GuruUjian::find($id);
        //   $ujian->nama_ujian  = $request->nama_ujian;
        //   $ujian->durasi      = $request->durasi;
        //   $ujian->jumlah_soal = $request->jumlah_soal;
        // $ujian->save();

        GuruUjian::where('id_ujian', $id)->update([
                 'nama_ujian' => $request->nama_ujian,
         'durasi' => $request->durasi,
         'jumlah_soal' => $request->jumlah_soal,
        ]);

        return back()->with('msg', '<b> <center>Ujian Berhasil Di Ubah<i class="mdi mdi-check"></i></b> </center> ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        GuruUjian::where('id_ujian', $id)->delete();
        return back()->with('msg', '<b> <center>Mata Pelajaran Berhasil Di Hapus <i class="mdi mdi-check"></i></b> </center> ');
    }
}
