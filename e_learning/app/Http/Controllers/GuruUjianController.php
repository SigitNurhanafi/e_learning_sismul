<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\MataPelajaran;
use App\GuruUjian;
use Auth;

class GuruUjianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matpel = MataPelajaran::select('id_matpel')
                     ->where('nama_matpel', '=', ''.Auth::user()->nama_matpel.'')
                     ->first();

        $ujian = GuruUjian::select('id_ujian', 'id_matpel', 'nama_ujian', 'durasi', 'jumlah_soal')
                 ->where('id_matpel', '=', ''.$matpel->id_matpel.'')
                 ->get();

        // $id_matpel = GuruUjian::select('id_ujian', 'id_matpel', 'nama_ujian', 'durasi', 'jumlah_soal')
        //          ->where('id_matpel', '=', ''.$id_matpel.'')
        //          ->first();

        // if (count($id_matpel)) {
        //     echo "wew";
        // } else {
        //     echo "ehhh";
        // }
        // dd(count($id_matpel));

        return view('layouts.guru.ujian.index', ['name'=> Auth::user()->name, 'id_matpel'=>$matpel->id_matpel,'ujian'=> $ujian, 'nama_matpel'=> Auth::user()->nama_matpel]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
       'id_matpel' => 'required',
       'nama_ujian' => 'required',
       'durasi' => 'required',
       'jumlah_soal' => 'required',
      ]);

        $ujian = new GuruUjian;
        $ujian->id_matpel = $request->id_matpel;
        $ujian->nama_ujian = $request->nama_ujian;
        $ujian->durasi = $request->durasi;
        $ujian->jumlah_soal = $request->jumlah_soal;
        $ujian->save();
        return back()->with('msg', '<b> <center>Soal Berhasil Ditambahkan<i class="mdi mdi-check"></i></b> </center> ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      //validasi form
        $this->validate($request, [
       'nama_ujian' => 'required',
       'durasi' => 'required',
       'jumlah_soal' => 'required',
      ]);

        //update mata pelajaran
        // $ujian = GuruUjian::find($id);
        //   $ujian->nama_ujian  = $request->nama_ujian;
        //   $ujian->durasi      = $request->durasi;
        //   $ujian->jumlah_soal = $request->jumlah_soal;
        // $ujian->save();

        GuruUjian::where('id_ujian', $id)->update([
                 'nama_ujian' => $request->nama_ujian,
         'durasi' => $request->durasi,
         'jumlah_soal' => $request->jumlah_soal,
        ]);

        return back()->with('msg', '<b> <center>Ujian Berhasil Di Ubah<i class="mdi mdi-check"></i></b> </center> ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        GuruUjian::where('id_ujian', $id)->delete();
        return back()->with('msg', '<b> <center>Mata Pelajaran Berhasil Di Hapus <i class="mdi mdi-check"></i></b> </center> ');
    }
}
