<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\MataPelajaran;
use App\Soal;
use Storage;
use Auth;

class SoalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //mengambil semua soal yang sudah dibuat
        $soal = MataPelajaran::select('soal.id', 'soal.pertanyaan', 'soal.jawaban_a', 'soal.jawaban_b', 'soal.jawaban_c', 'soal.jawaban_d', 'soal.jawaban_e', 'soal.kunci_jawaban', 'soal.file')
                ->join('Soal', 'mata_pelajaran.id_matpel', '=', 'soal.id_matpel')
                ->where('mata_pelajaran.nama_matpel', '=', ''.Auth::user()->nama_matpel.'')
                ->paginate(1);

        // return view('layouts.guru.mata_pelajaran.index', ['name'=> Auth::user()->name, 'mata_pelajaran'=> MataPelajaran::all()]);
        return view('layouts.guru.soal.index', ['name'=> Auth::user()->name, 'mata_pelajaran'=> $soal, 'nama_matpel'=> Auth::user()->nama_matpel]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id_mata_pelajaran = MataPelajaran::select('id_matpel')
                           ->where('nama_matpel', '=', ''.Auth::user()->nama_matpel.'')
                           ->get();

        $this->validate($request, [
       'pertanyaan' => 'required',
       'jawaban_a' => 'required',
       'jawaban_b' => 'required',
       'jawaban_c' => 'required',
       'jawaban_d' => 'required',
       'jawaban_e' => 'required',
       'kunci_jawaban' => 'required',
      ]);

        // if (count(MataPelajaran::where('nama_mata_pelajaran', $request->nama_matpel)
        //               ->where('jurusan', $request->jurusan)->get()) > 0) {
        //     return back()->with('msg', '<b><center>Mata Pelajaran  sudah ada<i class="mdi mdi-close"></i></center></b>');
        // } else {
        //     $mapel = new MataPelajaran;
        //
        //     $mapel->nama_mata_pelajaran = $request->nama_matpel;
        //     $mapel->jurusan = $request->jurusan;
        //
        //     $mapel->save();
        //     return back()->with('msg', '<b> <center>Mata Pelajaran Berhasil Ditambahkan<i class="mdi mdi-check"></i></b> </center> ');
        // }
        foreach ($id_mata_pelajaran as $key => $value) {
            $id_matpel = $value->id_matpel;
        }

        $mapel = new Soal;
        $mapel->id_matpel = $id_matpel;
        $mapel->pertanyaan = $request->pertanyaan;
        $mapel->jawaban_a = $request->jawaban_a;
        $mapel->jawaban_b = $request->jawaban_b;
        $mapel->jawaban_c = $request->jawaban_c;
        $mapel->jawaban_d = $request->jawaban_d;
        $mapel->jawaban_e = $request->jawaban_e;
        $mapel->jawaban_e = $request->jawaban_e;
        $mapel->kunci_jawaban = $request->kunci_jawaban;

        if ($request->file('img_soal')) {
            $get_extensions_file = explode('.', $request->file('img_soal')->getClientOriginalName());

            $get_extensions_file = end($get_extensions_file);

            $file_soal = time().'-'.$request->file('img_soal')->getClientOriginalName();

            Storage::disk('soal')->put($file_soal, file_get_contents($request->file('img_soal')->getRealPath()));
            $mapel->file = $file_soal;
        }

        $mapel->save();
        return back()->with('msg', '<b> <center>Soal Berhasil Ditambahkan<i class="mdi mdi-check"></i></b> </center> ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //mencari id mata pelajaran
        $id_mata_pelajaran = MataPelajaran::select('id_matpel')
                           ->where('nama_matpel', '=', ''.Auth::user()->nama_matpel.'')
                           ->get();
        //validasi form
        $this->validate($request, [
       'pertanyaan' => 'required',
       'jawaban_a' => 'required',
       'jawaban_b' => 'required',
       'jawaban_c' => 'required',
       'jawaban_d' => 'required',
       'jawaban_e' => 'required',
       'kunci_jawaban' => 'required',
      ]);

        //dd($request->all());

        $mapel = Soal::find($id);
        $mapel->pertanyaan = $request->pertanyaan;
        $mapel->jawaban_a = $request->jawaban_a;
        $mapel->jawaban_b = $request->jawaban_b;
        $mapel->jawaban_c = $request->jawaban_c;
        $mapel->jawaban_d = $request->jawaban_d;
        $mapel->jawaban_e = $request->jawaban_e;
        $mapel->kunci_jawaban = $request->kunci_jawaban;

        if ($request->file('img_soal')) {
            $get_extensions_file = explode('.', $request->file('img_soal')->getClientOriginalName());

            $get_extensions_file = end($get_extensions_file);

            $file_soal = time().'-'.$request->file('img_soal')->getClientOriginalName();

            Storage::disk('soal')->put($file_soal, file_get_contents($request->file('img_soal')->getRealPath()));
            $mapel->file = $file_soal;
        }

        //update mata pelajaran
        $mapel->save();

        return back()->with('msg', '<b> <center>Mata Pelajaran Berhasil Di Ubah<i class="mdi mdi-check"></i></b> </center> ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
