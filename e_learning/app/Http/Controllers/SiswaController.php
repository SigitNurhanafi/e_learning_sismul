<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SiswaDetail_Jawaban;
use App\Mata_pelajaran;
use App\Http\Requests;
use App\SiswaJawaban;
use App\Random_Soal;
use Carbon\Carbon;
use App\Ujian;
use App\Soal;
use Auth;
use DB;

class SiswaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $mata_pelajarans = Mata_pelajaran::select('ujian.id_ujian','ujian.durasi','ujian.jumlah_soal','ujian.nama_ujian','mata_pelajaran.nama_matpel','mata_pelajaran.jurusan')
        // ->join('ujian','mata_pelajaran.id_matpel','=','ujian.id_matpel')->get();

        //yang belum ujian
        $belum_mengikuti_ujian = Ujian::select('ujian.id_ujian', 'mata_pelajaran.nama_matpel', 'ujian.nama_ujian', 'ujian.durasi', 'ujian.jumlah_soal')
                               ->leftJoin('jawaban', 'jawaban.id_ujian', '=', 'ujian.id_ujian')
                               ->join('mata_pelajaran', 'mata_pelajaran.id_matpel', '=', 'ujian.id_matpel')
                               ->whereNull('jawaban.id')
                               ->where('mata_pelajaran.jurusan', '=', ''.Auth::user()->jurusan.'')
                               ->get();

        //yang belum ujian
        $sudah_mengikuti_ujian = Ujian::select('ujian.id_ujian', 'mata_pelajaran.nama_matpel', 'ujian.nama_ujian', 'ujian.durasi', 'ujian.jumlah_soal')
                               ->leftJoin('jawaban', 'jawaban.id_ujian', '=', 'ujian.id_ujian')
                               ->join('mata_pelajaran', 'mata_pelajaran.id_matpel', '=', 'ujian.id_matpel')
                               ->whereNotNull('jawaban.id')
                               ->where('mata_pelajaran.jurusan', '=', ''.Auth::user()->jurusan.'')
                               ->get();

        return view('layouts.siswa.ujian.index', ['name'=> Auth::user()->name, 'mata_pelajarans'=>$belum_mengikuti_ujian, 'sudah_mengikuti_ujian'=>$sudah_mengikuti_ujian,'ujian'=> Ujian::all()]);
    }


    public function mulai($id)
    {
        // date_default_timezone_set("Asia/Jakarta");
        $jumlah_soal = Ujian::select('jumlah_soal', 'id_matpel', 'durasi')
                     ->where('id_ujian', '=', ''.$id.'')
                     ->get();
        foreach ($jumlah_soal as $key => $value) {
            $jumlah = $value->jumlah_soal;
            $di_matpel = $value->id_matpel;
            $end_ujian = Carbon::now()->addMinutes($value->durasi);
            break;
        }
        // dd($end_ujian);
        //pengecekan apakah sudah terdapat random soal pada ujian tertentu
        $cek_random_soal = Random_Soal::select('id_random')
                         ->where('id_user', '=', ''.Auth::user()->id.'')
                         ->where('id_ujian', '=', ''.$id.'')
                         ->get();

        if (count($cek_random_soal) == 0) {
            $soals = Soal::select('id')
        ->where('id_matpel', '=', ''.$di_matpel.'')
        ->inRandomOrder()
        ->take($jumlah)
        ->get();


            foreach ($soals as $key => $value) {
                Random_Soal::insert(
              [
                'id_user' => Auth::user()->id,
                'id_soal' => $value->id,
                'id_ujian' => $id,
                'end_ujian' => $end_ujian,
              ]
           );
            }
            SiswaJawaban::insert(
            [
              'id' => Auth::user()->id,
              'id_ujian' => $id,
            ]
         );
        }

        //yang benar
        // $soals = Soal::select('soal.pertanyaan','mata_pelajaran.nama_matpel','soal.jawaban_a','soal.jawaban_b','soal.jawaban_c','soal.jawaban_d','soal.jawaban_e','soal.kunci_jawaban','soal.file')
        // ->join('mata_pelajaran','soal.id_matpel','=','mata_pelajaran.id_matpel')
        // ->join('ujian','ujian.id_matpel','=','mata_pelajaran.id_matpel')
        // ->where('ujian.id_ujian','=',''.$id.'')
        // ->paginate(1);

        $soal_random = Random_Soal::select('random_soal.end_ujian', 'random_soal.jawaban_siswa', 'random_soal.id_random', 'jawaban.id_jawaban', 'ujian.id_ujian', 'soal.id', 'soal.pertanyaan', 'mata_pelajaran.nama_matpel', 'soal.jawaban_a', 'soal.jawaban_b', 'soal.jawaban_c', 'soal.jawaban_d', 'soal.jawaban_e', 'soal.kunci_jawaban', 'soal.file')
                     ->join('soal', 'soal.id', '=', 'random_soal.id_soal')
                     ->join('users', 'users.id', '=', 'random_soal.id_user')
                     ->join('ujian', 'ujian.id_ujian', '=', 'random_soal.id_ujian')
                     ->join('mata_pelajaran', 'ujian.id_matpel', '=', 'mata_pelajaran.id_matpel')
                     ->join('jawaban', 'jawaban.id_ujian', '=', 'ujian.id_ujian')
                     ->where('random_soal.id_user', '=', ''.Auth::user()->id.'')
                     ->where('random_soal.id_ujian', '=', ''.$id.'')
                     ->paginate(1);

        //dd($soal_random);

        // $soals = Soal::select('soal.pertanyaan','mata_pelajaran.nama_matpel','soal.jawaban_a','soal.jawaban_b','soal.jawaban_c','soal.jawaban_d','soal.jawaban_e','soal.kunci_jawaban','soal.file')
        // ->join('mata_pelajaran','soal.id_matpel','=','mata_pelajaran.id_matpel')
        // ->inRandomOrder()
        // ->limit($jumlah)
        // ->paginate(1);


        return view('layouts.siswa.ujian.mulai', ['name'=> Auth::user()->name, 'soals'=>$soal_random, 'ujian'=> Ujian::all()]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function cek_jawaban(Request $request, $id)
    {
        //handle time out
        $randomSoal = Random_Soal::find($request->id_random);
        $dateEnd = Carbon::parse($randomSoal->end_ujian);
        $now = Carbon::now();
        $diff = $dateEnd->diffInSeconds($now, false);

        if ($diff > 0) {
            abort(404);
            // atau ridiret ke halamn waktu sudah habis
            // atau add flash sesion waktu udah abis
        }

        if ($request->jawaban == $request->kunci_jawaban) {
            $sekor = 1;
        } else {
            $sekor = 0;
        }
        //insert kedalam tabel detail jawaban untuk memberikan skor

        $cek_detail_jawaban = SiswaDetail_Jawaban::select('*')
                            ->where('id', '=', ''.$id.'')
                            ->where('id_jawaban', '=', ''.$request->id_jawaban.'')
                            ->get();

        if (count($cek_detail_jawaban) == 0) {
            //tambah data ke tabael detail jawaban untuk meberikan skor
            SiswaDetail_Jawaban::insert(
            [
              'id_jawaban' => $request->id_jawaban,
              'skor' => $sekor,
              'id' => $id,
            ]
         );
        } else {
            //update tabel soal random untuk mesingisakn jawaban users
            SiswaDetail_Jawaban::where('id_jawaban', $request->id_jawaban)
              ->where('id', $id)
              ->update(
                  ['skor' => $sekor]
         );
        }

        //update tabel soal random untuk mesingisakn jawaban users
        Random_Soal::where('id_random', $request->id_random)
            ->update(
                ['jawaban_siswa' => $request->jawaban]
       );
        return back();
    }

    public function lihat_nilai($id)
    {
        $nilai = SiswaJawaban::select('ujian.nama_ujian', DB::raw('round((100/ujian.jumlah_soal)*sum(detail_jawaban.skor)) nilai, ujian.jumlah_soal, sum(skor) jawaban_benar'))
                ->join('ujian', 'ujian.id_ujian', '=', 'jawaban.id_ujian')
                ->join('detail_jawaban', 'detail_jawaban.id_jawaban', '=', 'jawaban.id_jawaban')
                ->where('jawaban.id', '=', ''.Auth::user()->id.'')
                ->where('ujian.id_ujian', '=', ''.$id.'')
                ->get();

        return view('layouts.siswa.ujian.nilai', ['name'=> Auth::user()->name, 'nilai'=>$nilai, 'ujian'=> Ujian::all()]);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Random_Soal::find()
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
