<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\MataPelajaran;
use Auth;

class MataPelajaranController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.admin.mata_pelajaran.index', ['name'=> Auth::user()->name, 'mata_pelajaran'=> MataPelajaran::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
         'nama_matpel' => 'required|max:225',
         'jurusan' => 'required|max:150',
        ]);

        strtoupper($request->jurusan);

        if (count(MataPelajaran::where('nama_matpel', $request->nama_matpel)
                      ->where('jurusan', $request->jurusan)->get()) > 0) {
            return back()->with('msg', '<b><center>Mata Pelajaran  sudah ada<i class="mdi mdi-close"></i></center></b>');
        } else {
            $mapel = new MataPelajaran;

            $mapel->nama_matpel = strtolower($request->nama_matpel);
            $mapel->jurusan = strtoupper($request->jurusan);

            $mapel->save();
            return back()->with('msg', '<b> <center>Mata Pelajaran Berhasil Ditambahkan<i class="mdi mdi-check"></i></b> </center> ');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'nama_matpel' => 'required|max:225',
          'jurusan' => 'required|max:150',
        ]);

        $updateMapel = MataPelajaran::find($id);
        $updateMapel->nama_matpel = strtolower($request->nama_matpel);
        $updateMapel->jurusan = strtoupper($request->jurusan);
        $updateMapel->save();

        return back()->with('msg', '<b> <center>Mata Pelajaran Berhasil Di Ubah<i class="mdi mdi-check"></i></b> </center> ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (MataPelajaran::destroy($id)) {
            return back()->with('msg', '<b> <center>Mata Pelajaran Berhasil Di Hapus <i class="mdi mdi-check"></i></b> </center> ');
        }

        return abort(404);
    }
}
