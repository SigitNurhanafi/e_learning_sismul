<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Materi;
use App\MataPelajaran;

use Storage;
use Auth;

class MateriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $video_format = array("mp4", "3gp", "avi", "flv");
    public $document_format = array("doc", "pdf", "docx");

    public function index()
    {


        // dd(Materi::with('matpel')->get());
        $user = Auth::user();
        return view('layouts.guru.materi.index', ['guru'=>$user, 'name'=> Auth::user()->name, 'mata_pelajaran'=> MataPelajaran::all(), 'materi'=>Materi::with('matpel')->get(), 'video_format'=>$this->video_format, 'document_format'=>$this->document_format]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return 'metode create';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'materi'  => 'required|mimes:mp4,avi,3gp,mkv,doc,docx,pdf | max:10000000'
        ]);

        $matpel_guru = MataPelajaran::where('nama_matpel', Auth::user()->nama_matpel)->first();

        $get_extensions_file = explode('.', $request->file('materi')->getClientOriginalName());

        $get_extensions_file = end($get_extensions_file);

        $file_materi = time().'-'.$request->file('materi')->getClientOriginalName();

        Storage::disk('materi')->put($file_materi, file_get_contents($request->file('materi')->getRealPath()));

        $materi = new Materi;
        $materi->nama_materi = $request->nama_materi;
        $materi->id_matpel = $matpel_guru->id_matpel;
        $materi->deskripsi = $request->deskripsi;
        $materi->file = $file_materi;
        $materi->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = Auth::user();
        return view('layouts.siswa.materi.index', ['siswa'=>$user, 'name'=> Auth::user()->name, 'mata_pelajaran'=> MataPelajaran::all(), 'materi'=>Materi::with('matpel')->get(), 'video_format'=>$this->video_format, 'document_format'=>$this->document_format]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return 'methode edit';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'id_materi'=> 'required',
          'nama_materi'=> 'required',
          'deskripsi'=> 'required',
      ]);

        $updateMateri = Materi::find($id);
        $updateMateri->nama_materi = $request->nama_materi;
        $updateMateri->deskripsi = $request->deskripsi;
        $updateMateri->save();

        return back()->with('msg', '<b> <center>Materi Berhasil Di Ubah<i class="mdi mdi-check"></i></b> </center> ');
        // return $request->all();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete_materi = Materi::find($id);
        usleep(2500);
        Materi::destroy($id);

        unlink(public_path('storage\materi'.'\\'.$delete_materi->file));

        return back()->with('msg', 'delete');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function download($id)
    {
        $download_materi = Materi::find($id);
        return response()->download(public_path('storage/materi/'.$download_materi->file), $download_materi->nama_materi.'.'.explode('.', $download_materi->file)[1]);
    }
}
