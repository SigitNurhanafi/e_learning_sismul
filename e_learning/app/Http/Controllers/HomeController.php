<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use Carbon\Carbon;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::user()->is_permission;
        switch ($id) {


      case 1:
        // siswa
        return view('siswa')->with('name', Auth::user()->name);
        break;
      case 2:
        //guru
        return view('guru')->with('name', Auth::user()->name);
        break;
      case 3:

        return view('admin')->with('name', Auth::user()->name);
        break;
      }
    }
}
