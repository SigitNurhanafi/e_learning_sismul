<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\MataPelajaran;
use App\User;
use Auth;

class AddUsersContoller extends Controller
{
    public function indexAddGuru(Request $request)
    {
        return view('layouts.admin.tambah_guru.index', ['name'=> Auth::user()->name ,'mata_pelajaran'=> MataPelajaran::all()]);
    }

    public function indexAddSiswa(Request $request)
    {
        $UserSiswa = User::where('is_permission', 1)->get();
        return view('layouts.admin.tambah_siswa.index', ['name'=> Auth::user()->name ,'mata_pelajaran'=> MataPelajaran::all(), 'UserSiswa'=> $UserSiswa ]);
    }

    public function addGuru(Request $request)
    {
        $this->validate($request, [
          'name' => 'required|max:255',
          'email' => 'required|email|max:255|unique:users',
          'password' => 'required|min:6|confirmed',
          'mata_pelajaran' => 'required'
        ]);

        $matpelGuru = MataPelajaran::find($request->mata_pelajaran);

        // dd($matpelGuru);
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'is_permission' => 2,
            'jurusan' => strtoupper($matpelGuru->jurusan),
            'nama_matpel' => $matpelGuru->nama_matpel,
        ]);

        return back()->with('msg', 'Guru Berhasil Ditambahkan ');
    }

    public function editGuru(Request $request, $id)
    {
        return $request;
    }

    public function deleteGuru(Request $request, $id)
    {
        return $request;
    }

    public function addSiswa(Request $request)
    {
        $this->validate($request, [
          'name' => 'required|max:255',
          'email' => 'required|email|max:255|unique:users',
          'password' => 'required|min:6|confirmed',
          'jurusan' => 'required'
        ]);

        $matpelGuru = MataPelajaran::find($request->mata_pelajaran);

        // dd($matpelGuru);
        User::create([
          'name' => $request->name,
          'email' => $request->email,
          'password' => bcrypt($request->password),
          'is_permission' => 1,
          'jurusan' => strtoupper($request->jurusan),
        ]);

        return back()->with('msg', 'Siswa Berhasil Ditambahkan ');
    }

    public function editSiswa(Request $request, $id)
    {
        return $request;
    }

    public function deleteSiswa(Request $request, $id)
    {
        return $request;
    }
}
