<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\MataPelajaran;
use App\User;
use Auth;

class GuruMataPelajaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view(
            'layouts.admin.guru_pelajaran.index',
                    ['name'=> Auth::user()->name,
                    'jurusan'=> Auth::user()->jurusan,
                    'mata_pelajaran'=> MataPelajaran::all(),
                    'guru'=> User::where('is_permission', 2)->whereNotNull('nama_matpel')->get(),
                    'add_guru'=> User::where('is_permission', 2)->whereNull('nama_matpel')->get() ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'nama_guru' => 'required|max:225',
          'jurusan' => 'required|max:150',
        ]);

        $updateMapel = User::find($id);
        $updateMapel->name = $request->nama_guru;
        $updateMapel->jurusan = strtoupper($request->jurusan);
        $updateMapel->save();

        return back()->with('msg', '<b> <center>Mata Pelajaran Berhasil Di Ubah<i class="mdi mdi-check"></i></b> </center> ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (User::destroy($id)) {
            return back()->with('msg', '<b> <center>Mata Pelajaran Berhasil Di Hapus <i class="mdi mdi-check"></i></b> </center> ');
        }

        return abort(404);
    }
}
