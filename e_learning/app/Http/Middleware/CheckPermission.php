<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckPermission
{
    public function handle($request, Closure $next, $permission)
    {
        // $permission = explode('|', );

        if ($this->checkPermission($permission)) {
            return $next($request);
        }

        return abort(404);
    }

    private function checkPermission($permissions)
    {
        $userAccess = $this->getMyPermission(Auth::user()->is_permission);
        // foreach ($permissions as $key => $value) {
        if ($permissions == $userAccess) {
            return true;
        }
        // }
        return false;
    }


    private function getMyPermission($id)
    {
        switch ($id) {
        case 1:
          return 'siswa';
        break;
        case 2:
          return 'guru';
          break;
        case 3:
          return 'admin';
          break;
        }
    }
}
