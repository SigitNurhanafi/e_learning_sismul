<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Route::auth();

Route::get('/home', 'HomeController@index')->name('home-dashboard');

Route::group(['prefix' => 'get', 'middleware'=>'auth'], function () {
    Route::get('materi/{id}', ['as' => 'materi-download', 'uses' => 'MateriController@download']);
});

Route::group(['prefix' => 'admin', 'middleware'=>['auth','check-permission:admin']  ], function () {


    // Route::get('home', function () {
    //     return 'ini route admin pake Role Guru dan Pegawai';
    // })->name('admin-home');
    Route::get('/', function () {
        return redirect(route('home-dashboard'));
    });
    Route::get('mata-pelajaran', ['as' => 'admin-mata-pelajaran', 'uses' => 'MataPelajaranController@index']);
    Route::post('mata-pelajaran/store', ['as' => 'admin-mata-pelajaran-store', 'uses' => 'MataPelajaranController@store']);
    Route::post('mata-pelajaran/update/{id}', ['as' => 'admin-mata-pelajaran-update', 'uses' => 'MataPelajaranController@update']);
    Route::get('mata-pelajaran/delete/{id}', ['as' => 'admin-mata-pelajaran-delete', 'uses' => 'MataPelajaranController@destroy']);

    Route::get('guru-pelajaran', ['as' => 'guru-pelajaran', 'uses' => 'GuruMataPelajaranController@index']);
    Route::post('guru-pelajaran/store', ['as' => 'guru-pelajaran-store', 'uses' => 'GuruMataPelajaranController@store']);
    Route::post('guru-pelajaran/update/{id}', ['as' => 'guru-pelajaran-update', 'uses' => 'GuruMataPelajaranController@update']);
    Route::get('guru-pelajaran/delete/{id}', ['as' => 'guru-pelajaran-delete', 'uses' => 'GuruMataPelajaranController@destroy']);

    Route::get('guru/tambah', ['as' => 'tambah-guru', 'uses' => 'AddUsersContoller@indexAddGuru']);
    Route::get('siswa/tambah', ['as' => 'tambah-siswa', 'uses' => 'AddUsersContoller@indexAddSiswa']);

    Route::post('guru/tambah/create', ['as' => 'create-tambah-siswa', 'uses' => 'AddUsersContoller@addGuru']);
    Route::post('siswa/tambah/create', ['as' => 'create-tambah-guru', 'uses' => 'AddUsersContoller@addSiswa']);


    // Route::get('tambah-siswa', ['as' => 'tambah-siswa', 'uses' => 'AddUsersContoller@addSiswa']);
});



Route::group(['prefix' => 'guru', 'middleware'=>['auth','check-permission:guru']  ], function () {
    // Route::get('home', function () {
    //     return 'ini route admin pake Role Guru dan Pegawai';
    // })->name('admin-home');
    Route::get('/', function () {
        return redirect(route('home-dashboard'));
    });
    //untuk pengolahan soal
    Route::get('soal', ['as' => 'mata-pelajaran', 'uses' => 'SoalController@index']);
    Route::post('soal/store', ['as' => 'mata-pelajaran-store', 'uses' => 'SoalController@store']);
    Route::post('soal/update/{id}', ['as' => 'soal-update', 'uses' => 'SoalController@update']);

    //untuk pengolahan nilai
    Route::get('nilai', ['as' => 'guru-nilai', 'uses' => 'GuruNilaiController@index']);
    Route::get('nilai/lihat_siswa/{id}', ['as' => 'guru-lihat-siswa', 'uses' => 'GuruNilaiController@index_siswa']);
    Route::get('nilai/nilai_siswa/{id}', ['as' => 'nilai-siswa', 'uses' => 'GuruNilaiController@index_nilai_siswa']);

    //untuk pengolahan materi
    Route::get('materi', ['as' => 'mata-tamp', 'uses' => 'MateriController@index']);
    Route::post('materi/store', ['as' => 'upload-materi', 'uses' => 'MateriController@store']);
    Route::post('materi/update/{id}', ['as' => 'materi-update', 'uses' => 'MateriController@update']);
    Route::get('materi/delete/{id}', ['as' => 'materi-delete', 'uses' => 'MateriController@destroy']);


    //untuk pengolahan ujian
    Route::get('ujian', ['as' => 'guru-ujian', 'uses' => 'GuruUjianController@index']);
    Route::post('ujian/store', ['as' => 'ujian-store', 'uses' => 'GuruUjianController@store']);
    Route::post('ujian/update/{id}', ['as' => 'ujian-update', 'uses' => 'GuruUjianController@update']);
    Route::get('ujian/delete/{id}', ['as' => 'ujian-delete', 'uses' => 'GuruUjianController@destroy']);
});

Route::group([ 'prefix' => 'siswa' ,'middleware'=>['auth','check-permission:siswa']], function () {
    Route::get('/', function () {
        return redirect(route('home-dashboard'));
    });

    Route::get('ujian', ['as' => 'ujian', 'uses' => 'SiswaController@index']);
    Route::get('materi', ['as' => 'materi', 'uses' => 'MateriController@show']);
    Route::get('ujian-mulai/{id}', ['as' => 'ujian-mulai', 'uses' => 'SiswaController@mulai']);
    Route::post('jawab-soal/{id}', ['as' => 'jawab-soal', 'uses' => 'SiswaController@cek_jawaban']);
    Route::get('lihat_nilai/{id}', ['as' => 'lihat-nilai', 'uses' => 'SiswaController@lihat_nilai']);

    Route::get('update/password', ['as' => 'update-password', 'uses' => 'UserController@updatePassword']);
});
