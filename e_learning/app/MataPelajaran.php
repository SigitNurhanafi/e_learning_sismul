<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MataPelajaran extends Model
{
    public $timestamps = false;
    public $primaryKey = 'id_matpel';
    protected $table = 'mata_pelajaran';

    public function materi()
    {
        return $this->belongsTo('App\Materi', 'id_matpel');
    }
}
