<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuruUjian extends Model
{
    public $timestamps = false;
    protected $table = 'ujian';
    protected $fillable = ['id_matpel','nama_ujian','durasi','jumlah_soal'];
}
