<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiswaDetail_Jawaban extends Model
{
    public $timestamps = false;
    protected $table = 'detail_jawaban';
    protected $fillable =['id_jawaban','skor','id'];
}
