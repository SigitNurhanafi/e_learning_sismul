<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Random_Soal extends Model
{
    public $timestamps = false;
    public $primaryKey = 'id_random';
    protected $table = 'random_soal';
    protected $fillable =['id_user','id_soal','id_ujian','jawaban_siswa'];
}
