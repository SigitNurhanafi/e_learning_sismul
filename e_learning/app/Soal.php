<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Soal extends Model
{
    public $timestamps = false;
    protected $table = 'soal';
    protected $fillable = ['id_matpel','pertanyaan','jawaban_a','jawaban_b','jawaban_c','jawaban_d','jawaban_e','kunci_jawaban','file'];
}
