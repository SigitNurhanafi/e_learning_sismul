<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiswaJawaban extends Model
{
    public $timestamps = false;
    protected $table = 'jawaban';
    protected $fillable =['id_ujian','id'];
}
