<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materi extends Model
{
    public $timestamps = false;
    public $primaryKey = 'id_materi';
    protected $table = 'materi';
    protected $fillable = ['mata_pelajaran','deskripsi','file'];

    public function matpel()
    {
        return $this->belongsTo('App\MataPelajaran', 'id_matpel');
    }
}
